import {Alert} from 'react-native';
import {
    CHANGE_LANGUAGE,CHANGE_COLOR
} from './types';
export  function changeLanguage(change) {
    return  (dispatch) => {
        dispatch({ type: CHANGE_LANGUAGE,payload:change});  
    }
}

export  function changeColor(color) {
    return  (dispatch) => {
        dispatch({ type: CHANGE_COLOR,payload:color});  
    }
}