
import { SELECT_MENU,POP_ITEM,SHOW_MENU,HIDE_MENU } from "./types";



export  function selectMenu(item) {
    return dispatch => {
        dispatch({type: SELECT_MENU, payload: item})
    }
}

export  function removeItem(item) {
    return dispatch => {
        dispatch({type: POP_ITEM})
    }
}


export  function showMenu() {
    return dispatch => {
        dispatch({type: SHOW_MENU})
    }
}


export  function hideMenu() {
    return dispatch => {
        dispatch({type: HIDE_MENU})
    }
}