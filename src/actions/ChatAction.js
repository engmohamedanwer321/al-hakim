import axios from 'axios';
import Strigs from '../assets/strings';
import { BASE_END_POINT} from '../AppConfig';
import {
    CHAT,
    OPEN_IMAGE,
    CLOSE_IMAGE
} from './types';
import { RNToasty } from 'react-native-toasty';




export function getUnseenMessages(token) {
    console.log('un seen messages')
    return dispatch => {

        axios.get(`${BASE_END_POINT}messages/unseenCount`,{
            headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer ${token}`
            },
          })
        .then(response=>{
          console.log("UN SEEN")
          console.log(response.data.unseen)
          dispatch({type:CHAT,payload:response.data.unseen})       
        })
        .catch(error=>{
            console.log("UN SEEN ERROR")
          console.log(error.response)
        })
      
    }
}

export function openImage(open,image) {
  return dispatch => {
    
    dispatch({type:OPEN_IMAGE,payload:open,img:image})
  }
  
}



