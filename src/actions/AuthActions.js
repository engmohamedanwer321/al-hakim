//import { AsyncStorage } from 'react-native';
import AsyncStorage  from '@react-native-community/async-storage'
import axios from 'axios';
import {
   CURRENT_USER,USER_TOKEN,USER_LOCATION,LOGOUT
   } from './types';
import { BASE_END_POINT } from '../AppConfig';
import { RNToasty } from 'react-native-toasty';
import Strigs from '../assets/strings';
import {resetTo} from '../controlls/NavigationControll'



export function setUser(user){
  return dispatch => {
    dispatch({ type: CURRENT_USER, payload: user });
  }
}

export function logout(userToken,FirebaseToken) {
  return  dispatch => {
      const data = {
        token: FirebaseToken
      }
      axios.post(`${BASE_END_POINT}logout`,JSON.stringify(data),{
        headers:{
          "Authorization": `Bearer ${userToken}`,
          "Content-Type": "application/json"
        }
      })
      .then(response=>{
        console.log("LOGOUT DONE")
        resetTo('Login')
        //dispatch({type:LOGOUT})
        AsyncStorage.removeItem('USER')
      })
      .catch(error=>{
        console.log('ERROR   ',error.response)
      })
     
  }
}

export function userToken(token){
  return dispatch => {
    dispatch({type:USER_TOKEN,payload:token})
  }
}

export function userLocation(postion) {
  return dispatch => {
    console.log(postion)
    dispatch({type:USER_LOCATION,payload:postion})
  }
  
}

