import React,{Component} from 'react';
import {View,Alert,TouchableOpacity,Text} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon,Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import { BASE_END_POINT} from '../AppConfig';
import {getUnreadNotificationsCount} from '../actions/NotificationAction'
import {arrabicFont,englishFont,boldFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { push } from '../controlls/NavigationControll';


class ApllicationCard extends Component {
    
    state={
        status:''
    }

    componentDidMount(){
        const {data} = this.props;
        if(data.status=='created'){
            this.setState({status:Strings.created})
        }else if(data.status=='Specialist-Review'){
            this.setState({status:Strings.SpecialistReview})
        }
        /**  **/
        else if(data.status=='Additional-Uploads'){
            this.setState({status:Strings.AdditionalUploads})
        }
        /**  **/
        else if(data.status=='IR-CallConfirm'){
            this.setState({status:Strings.IRCallConfirm})
        }else if(data.status=='IR-Inprogress'){
            this.setState({status:Strings.IRInprogress})
        }else if(data.status=='IR-Created'){
            this.setState({status:Strings.IRCreated})
        }else if(data.status=='IR-Review'){
            this.setState({status:Strings.IRReview})
        }else if(data.status=='MDT-Review'){
            this.setState({status:Strings.MDTReview})
        }else if(data.status=='MDTR-Inprogress'){
            this.setState({status:Strings.MDTRInprogress})
        }else if(data.status=='MDTR-Created'){
            this.setState({status:Strings.MDTRCreated})
        }else if(data.status=='MDTR-Review'){
            this.setState({status:Strings.MDTRReview})
        }else if(data.status=='Consultant-CallConfirm'){
            this.setState({status:Strings.ConsultantCallConfirm})
        }else if(data.status=='Survey'){
            this.setState({status:Strings.Survey})
        }else if(data.status=='Completed'){
            this.setState({status:Strings.Completed})
        }
    }

    render(){
        const {isRTL,data,currentUser} = this.props;
        const {status} = this.state
        return(
            <TouchableOpacity 
            onPress={()=>{
                /*
                // if status = created go to make order page step 2 
                if(data.status=='created'||data.status=='Additional-Uploads'){
                    const application = {
                        page:2,
                        applicationId:data.id,
                        applicationsStatus:data.status=='Additional-Uploads'?data.status:null,
                    }
                    push('MakeApplication',application)
                }

                // if status = created go to DetermineApplicationContactTimeWithSpecialist page
                if(data.status=='Specialist-Review'){
                    const application = {
                        applicationId:data.id,
                    }
                    push('DetermineApplicationContactTimeWithSpecialist',application)
                }

                // if status = created go to DetermineApplicationContactTimeWithSpecialist page
                if(data.status=='IR-CallConfirm'){
                    const application = {
                        applicationId:data.id,
                        hideButtons:true,
                    }
                    push('DetermineApplicationContactTimeWithSpecialist',application)
                }

                // if status = IR-Inprogress go to SpecialListReviewApplication page
                if(data.status=='IR-Inprogress'){
                    push('SpecialListReviewApplication')
                }

                // if status = IR-Created go to InitialReportCreated page
                if(data.status=='IR-Created'){
                    push('InitialReportCreated',data)
                }

                
                // if status = IR-Review go to ShowIntialRoport page 
                if(data.status=='IR-Review'){
                    push('ShowIntialRoport',data)
                }


                
                // if status = MDTR-Review go to show final report page 
                if(data.status=='MDT-Review'){
                    push('MdtReview',data)
                }

                // if status = MDTR-Inprogress go to show final report page 
                if(data.status=='MDTR-Inprogress'){
                    push('ConsuatantlReviewApplication')
                }

                // if status = MDTR-Created go to MdtReportCreated page 
                if(data.status=='MDTR-Created'){
                    push('MdtReportCreated',data)
                }

                // if status = MDTR-Review go to ShowFinalRoport page 
                if(data.status=='MDTR-Review'){
                    push('ShowFinalRoport',data)
                }

                // if status = Consultant-Review go to DetermineApplicationContactTimeWithConsultant page 
                if(data.status=='Consultant-Review'){
                    push('DetermineApplicationContactTimeWithConsultant',{applicationId:data.id})
                }

                // if status = Consultant-CallConfirm go to DetermineApplicationContactTimeWithConsultant page 
                if(data.status=='Consultant-CallConfirm'){
                    push('DetermineApplicationContactTimeWithConsultant',{applicationId:data.id,hideButtons:true})
                }

                // if status = Survey go to Rate page 
                if(data.status=='Survey'){
                    push('Rate',{applicationId:data.id})
                }

                // if status = Completed go to Rate page 
                if(data.status=='Completed'){
                    push('ShowAllReports',data)
                }

               */
            }}> 
            <Animatable.View 
            style={{paddingVertical:moderateScale(10), flexDirection:isRTL?'row-reverse':'row',backgroundColor:'white', borderBottomWidth:1,borderBottomColor:'#cccbcb', marginTop:moderateScale(0), alignSelf:'center', width:responsiveWidth(90)}}
            animation={"flipInX"} duration={300}
            >
                
                <FastImage 
                source={require('../assets/imgs/applicationIcon.png')}
                resizeMode='contain'
                style={{width:wp(12),height:responsiveHeight(7)}}
                />

                <View style={{flex:1,}}>
                    <View style={{alignItems:'center', justifyContent:'space-between', flexDirection:isRTL?'row-reverse':'row',width:wp(78)}}>
                        <Text style={{width:wp(45),textAlign:isRTL?'right':'left',fontSize:responsiveFontSize(7),  color:"black", fontFamily:boldFont }}>{data.name}</Text>
                        <Text style={{width:wp(30),textAlign:isRTL?'left':'right', fontSize:responsiveFontSize(6),  color:"#946AB3", fontFamily:boldFont }}>{status}</Text>
                    </View>
                    {/*<Text style={{width:wp(78),marginTop:moderateScale(2), textAlign:isRTL?'right':'left', fontSize:responsiveFontSize(6),  color:colors.grayFont, fontFamily:isRTL?arrabicFont:englishFont }}>Nonstop 1h 47m YUL - JFK</Text>
                    <Text style={{width:wp(78),marginTop:moderateScale(2), textAlign:isRTL?'right':'left', fontSize:responsiveFontSize(6),  color:colors.grayFont, fontFamily:isRTL?arrabicFont:englishFont }}>Skyview Airlines</Text>*/}
                </View>
                     
            </Animatable.View>
        </TouchableOpacity>
           
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
});

const mapDispatchToProps = {
    getUnreadNotificationsCount
}


export default connect(mapStateToProps,mapDispatchToProps)(ApllicationCard);
