import React,{Component} from 'react';
import {BackHandler,Modal, View,Alert,TouchableOpacity,Text,Platform,ImageBackground} from 'react-native';
import { moderateScale, responsiveWidth, responsiveHeight, responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";
import * as colors from '../assets/colors'
import Strings from '../assets/strings';
import { Thumbnail,Icon,Button } from 'native-base';
import moment from 'moment'
import "moment/locale/ar"
import axios from 'axios';
import {pop,openSideMenu,push,resetTo } from '../controlls/NavigationControll'
import { BASE_END_POINT} from '../AppConfig';
import {getUnreadNotificationsCount} from '../actions/NotificationAction'
import {arrabicFont,englishFont,boldFont} from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import FastImage from 'react-native-fast-image'
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import {hideMenu} from '../actions/MenuActions'
import AsyncStorage from '@react-native-community/async-storage'
import { setUser } from '../actions/AuthActions'


class Menu extends Component {

    state={
        about:false,
    }

    logout = () =>{
        console.log("LOGOUT 1")
        AsyncStorage.removeItem("USER")
        this.props.setUser(null) 
        resetTo('Login') 
        //this.props.hideMenu()
        console.log("LOGOUT 2")
    }

    menuItem = (text,img,page,isLogout,about) =>{
        return(
            <TouchableOpacity 
            onPress={()=>{
                if(isLogout){
                    this.props.hideMenu()
                    this.logout()
                    console.log("ss LOGOUT")
                    
                }else{
                    if(about){
                        this.setState({about:true})
                    }else{
                        push(page)
                        this.props.hideMenu()
                    }
                }
                
            }}
            style={{width:wp(27), alignItems:'center'}}>
                <FastImage
                resizeMode='contain'
                source={img}
                style={{width:wp(11),height:responsiveHeight(8)}}
                />

                <Text style={{textAlign:'center', fontSize:16,  color:colors.darkGray, fontFamily:englishFont }}>{text} </Text>
            </TouchableOpacity>
        )
    }
    
    render(){
        const {isRTL,currentUser,showMenuModal} = this.props;
        const {about}= this.state
        return(
            <Modal
            visible={showMenuModal}
            animationType='slide'
            onRequestClose={()=>{
                this.props.hideMenu()
            }}
             >
            
            <ImageBackground
             source={require('../assets/imgs/bluredBackground.jpg')}
            onPress={()=>{this.props.hideMenu()}}
             style={{zIndex:100000,elevation:10, width:wp(100),height:responsiveHeight(100),backgroundColor:'rgba(1,1,1,.9)', position:'absolute',top:0,left:0}}
             >

                 
            <TouchableOpacity
            onPress={()=>{
                if(about){
                    this.setState({about:false})
                }else{
                    this.props.hideMenu()
                }
            }}
             style={{ marginTop:moderateScale(10),marginHorizontal:moderateScale(6),alignSelf:isRTL?'flex-end':'flex-start'}}>
                <Icon name={isRTL?'arrowright':'arrowleft'} type='AntDesign' style={{fontSize:responsiveFontSize(20),color:'black'}} />
            </TouchableOpacity>

                <View style={{justifyContent:'center',alignItems:'center',height:responsiveHeight(92),}}>
                 
                 <TouchableOpacity activeOpacity={1} style={{paddingVertical:moderateScale(5), alignSelf:'center', justifyContent:'center',alignItems:'center', zIndex:1000, width:wp(90),backgroundColor:'white',borderRadius:moderateScale(20)}}>
                   
                    {about?

                    <View style={{alignSelf:'center',width:wp(85),flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',}}>
                        {this.menuItem(Strings.aboutElkhabir,require('../assets/imgs/aboutIcon.png'),"AboutElkabir")}
                        {this.menuItem(Strings.aboutABC,require('../assets/imgs/aboutIcon.png'),"AboutAbc")}
                        {this.menuItem(Strings.abcTeam,require('../assets/imgs/abcTeamIcon.png'),"AbcTeam")}
                    </View>
                    :
                    <View>
                        <View style={{alignSelf:'center',width:wp(85),flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}}>
                            {this.menuItem(Strings.about,require('../assets/imgs/aboutIcon.png'),"AboutElkabir")}
                            
                        </View>
                        
                        <View  style={{marginVertical:moderateScale(5),width:wp(80),alignSelf:'center', height:1,backgroundColor:colors.lightGray }} />
                   
                        <View style={{alignSelf:'center',width:wp(85),flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',}}>
                            {this.menuItem(Strings.editProfile,require('../assets/imgs/editProfileicon.png'),'UpdateMyProfile')} 
                            {this.menuItem(Strings.contactUs,require('../assets/imgs/contactUsIcon.png'),"ContactUs")}
                            {this.menuItem(Strings.language,require('../assets/imgs/aboutIcon.png'),"SelectLanguage")}
                            
                        </View>

                        <View  style={{marginVertical:moderateScale(5),marginTop:moderateScale(10),width:wp(80),alignSelf:'center', height:1,backgroundColor:colors.lightGray }} />
                        
                        <View style={{alignSelf:'center',width:wp(85),flexDirection:isRTL?'row-reverse':'row',justifyContent:'center',alignItems:'center'}}>
                            {this.menuItem(Strings.logout,require('../assets/imgs/abcTeamIcon.png'),"",true)}
                        </View>
                        
                        

                        
                    </View>
                    }
                    
                
                 </TouchableOpacity>
                </View>
            </ImageBackground>
            </Modal>
        );
    }
}


const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
    currentUser: state.auth.currentUser,
    showMenuModal:state.menu.showMenuModal
});

const mapDispatchToProps = {
    getUnreadNotificationsCount,
    hideMenu,
    setUser,
}


export default connect(mapStateToProps,mapDispatchToProps)(Menu);
