import React, { Component } from 'react';
import {
  View,
  Text, Alert,
  Image,
  ActivityIndicator,
  Platform,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  Share
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage'
import { connect } from 'react-redux';
import { Button, Content, Icon, Thumbnail } from 'native-base';
import { bindActionCreators } from 'redux';
import * as Actions from '../actions/MenuActions';
import MenuItem from '../common/MenuItem';
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import * as colors from '../assets/colors';
import { rootNavigator } from '../screens/Login';
import Strings from '../assets/strings';
import { selectMenu } from '../actions/MenuActions';
import strings from '../assets/strings';
import { RNToasty } from 'react-native-toasty';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { openSideMenu, push, resetTo } from '../controlls/NavigationControll'
import { arrabicFont, englishFont } from '../common/AppFont'
import { logout } from '../actions/AuthActions'


class MenuContent extends Component {


  state = {
    v: false,
    load: false,
    showLogoutDialog: false,
  }

  render() {
    const { item, currentUser, color, isRTL } = this.props;
    if (this.props.isRTL) {
      Strings.setLanguage('ar')
    } else {
      Strings.setLanguage('en')
    }
    console.log("menu Content item =>   " + item);

    return (
      <>
        <View style={{ backgroundColor: colors.darkGreen, width: wp(130), height: Platform.OS === 'ios' ? responsiveHeight(3) : 0 }} />
       
      
        <View style={{aliginSelf:'center', backgroundColor: colors.white, width:wp(100), height: responsiveHeight(100)}}>
{/* 
          <View
            onPress={() => {
              if (this.props.currentUser) {
                if (item[item.length - 1] == "PROFILE") {
                  openSideMenu(false)
                } else {
                  openSideMenu(false)
                  this.props.selectMenu('PROFILE');
                  push('Profile')
                }
              } else {
                push('Login')
                openSideMenu(false)
              }
            }}
            style={{ marginBottom: moderateScale(12), paddingVertical: moderateScale(8), alignSelf: 'center', width: responsiveWidth(82), backgroundColor: colors.darkGreen, justifyContent: 'center', alignItems: 'center' }}>

            <View style={{ alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
              <Thumbnail
                style={{ borderColor: colors.buttonColor }} large
                source={currentUser && currentUser.user.img ? { uri: currentUser.user.img } : require('../assets/imgs/profileicon.jpg')} />
            </View>

            {true &&
              <View style={{ marginTop: moderateScale(4), alignSelf: 'center', justifyContent: 'center' }}>
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: 'white', fontSize: responsiveFontSize(8) }}>{currentUser && currentUser.user.firstname + " " + currentUser.user.lastname}</Text>
              </View>
            }

            {true &&
              <View style={{ marginTop: moderateScale(2), alignSelf: 'center', justifyContent: 'center' }}>
                <Text style={{ fontFamily: isRTL ? arrabicFont : englishFont, color: 'white', fontSize: responsiveFontSize(6) }}>{currentUser && currentUser.user.phone}</Text>
              </View>
            }

          </View>
      currentUser && currentUser.user.type == 'USER' &&
      <MenuItem
        onPress={() => {
          openSideMenu(false)
          push('History')
        }}
        title={Strings.history}
        img={require('../assets/imgs/home-history-icon.png')}
      />
      */}
                

         
        

        </View>
      </>
    );
  }
}


const mapStateToProps = state => ({
  item: state.menu.item,
  isRTL: state.lang.RTL,
  currentUser: state.auth.currentUser,
  userToken: state.auth.userToken,
});

const mapDispatchToProps = {
  selectMenu,
  logout
}

export default connect(mapStateToProps, mapDispatchToProps)(MenuContent);
// MenuContent
// connect(mapStateToProps, mapDispatchToProps, )(MenuContent);
