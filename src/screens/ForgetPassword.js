import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, FlatList, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import {isEmail} from '../controlls/Validations'
import moment from 'moment'
import Dialog, {DialogTitle, DialogContent,SlideAnimation } from 'react-native-popup-dialog';



class ForgetPassword extends Component {

    state = {
        email: ' ',
        emailError:null,
        showDialog:false,
    }

    componentDidMount() {
        enableSideMenu(false, null)
    }

    title = () => {
        const { isRTL } = this.props
        return (
            <Text style={{alignSelf:'center',marginTop:moderateScale(5), fontSize:responsiveFontSize(24),  color: 'black', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.forget}</Text>
        )
    }

    backButton = () => {
        const { isRTL } = this.props
        return (
            <TouchableOpacity
            onPress={()=>{pop()}}
             style={{alignSelf:isRTL?'flex-end':'flex-start', marginTop:moderateScale(13),marginHorizontal:moderateScale(10)}}>
                <Icon name={isRTL?'arrowright':'arrowleft'} type='AntDesign' style={{fontSize:responsiveFontSize(20),color:'black'}} />
            </TouchableOpacity>
        )
    }


    emailInput = () => {
        const { isRTL } = this.props
        const { email,emailError } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.email}</Label>    
                    <Input
                        keyboardType='email-address'
                        onChangeText={(val) => { 
                            this.setState({ email: val })
                            if(val){
                               this.setState({emailError:null}) 
                            }else{
                                this.setState({emailError:Strings.require}) 
                            }
                            /*if(!isEmail(val)){
                                this.setState({emailError:Strings.emailFormatInvalid})
                            }*/
                         }}
                        style={{fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.email}
                    />
                </Item>

                {emailError&&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {emailError}</Text>
                }
            </View>
        )
    }

    forget = () => {
        const {email } = this.state
        
        if(!isEmail(email)){
            this.setState({emailError:Strings.emailFormatInvalid})
        }
        if (!email.replace(/\s/g, '').length) {
            this.setState({emailError:Strings.require})
        }  
       

        if (email.replace(/\s/g, '').length && isEmail(email) ) {
            this.setState({ loading: true })
            
            var data= new URLSearchParams()
            data.append('email',email)
           
            axios.post(`${BASE_END_POINT}users/forget_password`, data, {
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded'
                }
            })
            .then(response => {
                this.setState({showDialog:true, loading: false })
                console.log(response.data)
               
            })
            .catch(error => {
                console.log("Error   ",error.response)
                console.log("status   ",error.response.status)
                this.setState({ loading: false })
                
                if (error.response.status == 404) {
                    RNToasty.Error({ title: Strings.emailNotFound })
                }
            })
        }

    }

    forgetButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => {
                this.forget()
             }} style={{flexDirection:isRTL?'row-reverse':'row', alignSelf: 'center', marginVertical: moderateScale(15), height: responsiveHeight(7), width: wp(80), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(5) }} >
                <Text style={{fontSize:responsiveFontSize(8),marginHorizontal:moderateScale(2),  color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.send}</Text>
            </TouchableOpacity>
        )
    }


    dialog=()=>{
        const { isRTL } = this.props
        return (
            <Dialog
            visible={this.state.showDialog}
            dialogAnimation={new SlideAnimation({
              slideFrom: 'bottom',
            })}
            dialogTitle={<DialogTitle title={Strings.elkhabir}/>}
            width={wp(80)}
            height={responsiveHeight(55)}
          >
            <View style={{width:wp(80),height:responsiveHeight(55)}}>
                <FastImage
                resizeMode='contain'
                source={require('../assets/imgs/appLogo.png')}
                style={{width:wp(80),height:responsiveHeight(20)}}
                />

                <Text style={{marginVertical:moderateScale(5), alignSelf:'center',width:wp(60),textAlign:'center', fontSize:responsiveFontSize(8),marginHorizontal:moderateScale(2),  color: 'black', fontFamily: boldFont }}>{Strings.mustBeChange}</Text>
                
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({showDialog:false})
                    resetTo("Login")
                    //push('MakeApplication')
                }} 
                style={{borderRadius:moderateScale(5), width:wp(40),marginTop:moderateScale(6), alignSelf:'center',height:responsiveHeight(8),backgroundColor:'#946AB3',justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(2),  color: colors.white, fontFamily: boldFont }}>{Strings.ok}</Text>
                </TouchableOpacity>

            </View>
          </Dialog>
        ) 
    }
  


    render() {
        const { loading} = this.state
        return (
            <View style={{backgroundColor:colors.grayBackground,flex:1}}>
                    {this.backButton()}
                    {this.title()}
                    
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                        {this.emailInput()}
                        {this.forgetButton()}
                    </View>
                {this.dialog()}
                {loading &&
                    <LoadingDialogOverlay title={Strings.wait} />
                }

            

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,

    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(ForgetPassword);

