import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppCommanHeader from '../components/AppCommanHeader'
import Menu from '../components/Menu'
import { WebView } from 'react-native-webview';

class AboutAbc extends Component {

    
    componentDidMount() {
        enableSideMenu(false, null)
    }

    render() {
        const {isRTL} = this.props
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                
                <AppCommanHeader title={Strings.aboutABC} />
          
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{alignSelf:isRTL?"flex-end":'flex-start',marginTop:moderateScale(5), marginHorizontal:moderateScale(6),flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'center'}}>
                        <FastImage
                            source={require('../assets/imgs/profileicon.jpg')}
                            style={{borderWidth:3,borderColor:'#946AB3',width:30,height:30,borderRadius:15}}
                        />
                        <Text style={{fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(2),  color: colors.darkBlue, fontFamily: boldFont }}>Elkhabir</Text>
                    </View>

                   

                    <View style={{padding:moderateScale(10),paddingBottom:moderateScale(40), backgroundColor:"#946AB3",width:wp(90),borderRadius:moderateScale(9),alignSelf:'center',marginTop:moderateScale(5),marginBottom:moderateScale(20)}} >
                        
                    <WebView 
                        javaScriptEnabled
                        allowsFullscreenVideo
                        style={{width:wp(80),height:responsiveHeight(38)}}
                        source={{ uri: 'https://www.youtube.com/embed/NlqAhek_DzY' }} 
                        />
                    

                        <Text style={{marginTop:moderateScale(20),fontSize:responsiveFontSize(9), color:'white', fontFamily:isRTL?arrabicFont:englishFont }}>
                        ABC was founded by a group 
                        of experts who were trained
                        in Europe and the United States. 
                        {'\n'}{'\n'}
                        Observing the patterns for 
                        care of breast cancer and 
                        services provided abroad, 
                        we wanted to implement the
                        same standards in Egypt
                        {'\n'}{'\n'}
                        With this international 
                        background and experience, 
                        and knowing well the requirements
                        and the different nature of breast
                        cancer patients in Egypt we wanted
                        to apply excellence in our care scented 
                        by Arab and Egyptian hospitality.
                        {'\n'}{'\n'}
                        Our multidisciplinary team is 
                        determined to offer efficient 
                        practice to our patients. 
                        We are dedicated to deliver 
                        the best for our patients 
                        health care through making 
                        a combined decision rather 
                        than individual opinions. 
                        </Text>
                    </View>

                </ScrollView>



                {/*this.props.showMenuModal&&<Menu/>*/}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(AboutAbc);

