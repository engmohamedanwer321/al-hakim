import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  ScrollView,
  TextInput,
  Alert,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import {Icon, Button, Item, Input, Label} from 'native-base';
import {setUser} from '../actions/AuthActions';
import {
  responsiveHeight,
  responsiveWidth,
  moderateScale,
  responsiveFontSize,
} from '../utils/responsiveDimensions';
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import {
  enableSideMenu,
  resetTo,
  push,
  pop,
} from '../controlls/NavigationControll';
import {arrabicFont, englishFont, boldFont} from '../common/AppFont';
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import {removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image';
import {RNToasty} from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {BASE_END_POINT} from '../AppConfig';
import strings from '../assets/strings';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import AppHomeHeader from '../components/AppHomeHeader';
import Menu from '../components/Menu';
import Dialog, {
  DialogTitle,
  DialogContent,
  SlideAnimation,
} from 'react-native-popup-dialog';

class AdminstraionHome extends Component {
 
 

  render() {
    const {isRTL,currentUser} = this.props;
    return (
      <View style={{backgroundColor: colors.lightGray, flex: 1}}>
        <AppHomeHeader hideBack />

        <View
          style={{
            alignSelf: 'flex-end',marginTop: moderateScale(5),marginHorizontal: moderateScale(6),flexDirection: isRTL ? 'row-reverse' : 'row',alignItems: 'center', justifyContent: 'center',}}
          >
          <Text
            style={{
              fontSize: responsiveFontSize(6),
              marginHorizontal: moderateScale(2),
              color: colors.darkBlue,
              fontFamily: boldFont,
            }}>
            Elkhabir
          </Text>
          <FastImage
            source={require('../assets/imgs/profileicon.jpg')}
            style={{
              borderWidth: 3,
              borderColor: '#946AB3',
              width: 30,
              height: 30,
              borderRadius: 15,
            }}
          />
        </View>

        <Animatable.View animation="slideInRight" delay={400}>
          <TouchableOpacity
            style={{width: wp(85),marginTop: moderateScale(10),alignSelf: 'flex-end',borderTopLeftRadius: moderateScale(10),borderBottomLeftRadius: moderateScale(10),height: responsiveHeight(8),backgroundColor: '#946AB3',justifyContent: 'center',alignItems: 'center', }}
            >
            <Text
              style={{textAlign: isRTL ? 'right' : 'left',fontSize: responsiveFontSize(7),marginHorizontal: moderateScale(2), color: colors.white,fontFamily: englishFont,}}
              >
              {Strings.help}
            </Text>
          </TouchableOpacity>
        </Animatable.View>

        <TouchableOpacity
          onPress={() => {push('UploadReports')}}
          style={{borderRadius: moderateScale(10), width: wp(70),marginTop: moderateScale(15),alignSelf: 'center',height: responsiveHeight(8),backgroundColor: colors.darkBlue,justifyContent: 'center',alignItems: 'center',}}
          >
          <Text style={{fontSize: responsiveFontSize(6),marginHorizontal: moderateScale(2),color: colors.white,fontFamily: boldFont,}}>
            {Strings.uploadFile}
          </Text>
        </TouchableOpacity>

       

        


        {this.props.showMenuModal && <Menu />}
      </View>
    );
  }
}
const mapDispatchToProps = {
  setUser,
  removeItem,
};

const mapToStateProps = state => ({
  isRTL: state.lang.RTL,
  showMenuModal: state.menu.showMenuModal,
  currentUser: state.auth.currentUser,
  //userToken: state.auth.userToken,
});

export default connect(
  mapToStateProps,
  mapDispatchToProps,
)(AdminstraionHome);
