import { Navigation } from 'react-native-navigation';
import { HOC } from '../controlls/NavigationControll'

import ForgetPassword from './ForgetPassword'
import SplashScreen from './SplashScreen';
import MenuContent from "../components/MenuContent";
import Login from './Login';
import SelectLanguage from './SelectLanguage';
import ContactUs from './ContactUs'
import Notifications from './Notifications'
import Register from './Register'
import UpdateMyProfile from './UpdateMyProfile'
import Home from './Home'
import AboutElkabir from './AboutElkabir'
import AbcTeam from './AbcTeam'
import AboutAbc from './AboutAbc'
import MdtTeam from './MdtTeam'
import MdtPreparatoryClinc from './MdtPreparatoryClinc'
import OncolplasticBreastReconstructionClinc from './OncolplasticBreastReconstructionClinc'
import BreastImagingTeam from './BreastImagingTeam'
import ChemoTeam from './ChemoTeam'
import Laboratories from './Laboratories'
import PreoperativeAssessmentTeam from './PreoperativeAssessmentTeam'
import EarlyDetectionAndGeneticsClinic from './EarlyDetectionAndGeneticsClinic'
import FunctionalGynaecologyClinic from './FunctionalGynaecologyClinic'
import PsychologyClinic from './PsychologyClinic'
import PhysiotherapyAndLymphDrainageClinic from './PhysiotherapyAndLymphDrainageClinic'
import MyApplications from './MyApplications'
import SpecialListReviewApplication from './SpecialListReviewApplication'
import MakeApplication from './MakeApplication'
import Payment from './Payment'
import ContactUs2 from './ContactUs2'
import ShowIntialRoport from './ShowIntialRoport'
import DetermineApplicationContactTimeWithSpecialist from './DetermineApplicationContactTimeWithSpecialist'
import DetermineApplicationContactTimeWithConsultant from './DetermineApplicationContactTimeWithConsultant'
import ShowFinalRoport from './ShowFinalRoport'
import ConsuatantlReviewApplication from './ConsuatantlReviewApplication'
import Rate from './Rate'
import InitialReportCreated from './InitialReportCreated'
import MdtReview from './MdtReview'
import MdtReportCreated from './MdtReportCreated'
import ShowAllReports from './ShowAllReports'
import About from './About'
import AdminstraionHome from './AdminstraionHome'
import UploadReports from './UploadReports'

export default function registerScreens() {
     Navigation.registerComponent("UploadReports", () => HOC(UploadReports), () => UploadReports);
     Navigation.registerComponent("AdminstraionHome", () => HOC(AdminstraionHome), () => AdminstraionHome);
     Navigation.registerComponent("About", () => HOC(About), () => About);
     Navigation.registerComponent("ForgetPassword", () => HOC(ForgetPassword), () => ForgetPassword);
     Navigation.registerComponent("ShowAllReports", () => HOC(ShowAllReports), () => ShowAllReports);
     Navigation.registerComponent("MdtReportCreated", () => HOC(MdtReportCreated), () => MdtReportCreated);
     Navigation.registerComponent("MdtReview", () => HOC(MdtReview), () => MdtReview);
     Navigation.registerComponent("InitialReportCreated", () => HOC(InitialReportCreated), () => InitialReportCreated);
     Navigation.registerComponent("Rate", () => HOC(Rate), () => Rate);
     Navigation.registerComponent("ConsuatantlReviewApplication", () => HOC(ConsuatantlReviewApplication), () => ConsuatantlReviewApplication);
     Navigation.registerComponent("ShowFinalRoport", () => HOC(ShowFinalRoport), () => ShowFinalRoport);
     Navigation.registerComponent("DetermineApplicationContactTimeWithConsultant", () => HOC(DetermineApplicationContactTimeWithConsultant), () => DetermineApplicationContactTimeWithConsultant);
     Navigation.registerComponent("DetermineApplicationContactTimeWithSpecialist", () => HOC(DetermineApplicationContactTimeWithSpecialist), () => DetermineApplicationContactTimeWithSpecialist);
     Navigation.registerComponent("ShowIntialRoport", () => HOC(ShowIntialRoport), () => ShowIntialRoport);
     Navigation.registerComponent("ContactUs2", () => HOC(ContactUs2), () => ContactUs2);
     Navigation.registerComponent("Payment", () => HOC(Payment), () => Payment);
     Navigation.registerComponent("MakeApplication", () => HOC(MakeApplication), () => MakeApplication);
     Navigation.registerComponent("SpecialListReviewApplication", () => HOC(SpecialListReviewApplication), () => SpecialListReviewApplication);
     Navigation.registerComponent("MyApplications", () => HOC(MyApplications), () => MyApplications);
     Navigation.registerComponent("PhysiotherapyAndLymphDrainageClinic", () => HOC(PhysiotherapyAndLymphDrainageClinic), () => PhysiotherapyAndLymphDrainageClinic);
     Navigation.registerComponent("PsychologyClinic", () => HOC(PsychologyClinic), () => PsychologyClinic);
     Navigation.registerComponent("FunctionalGynaecologyClinic", () => HOC(FunctionalGynaecologyClinic), () => FunctionalGynaecologyClinic);
     Navigation.registerComponent("EarlyDetectionAndGeneticsClinic", () => HOC(EarlyDetectionAndGeneticsClinic), () => EarlyDetectionAndGeneticsClinic);
     Navigation.registerComponent("PreoperativeAssessmentTeam", () => HOC(PreoperativeAssessmentTeam), () => PreoperativeAssessmentTeam);
     Navigation.registerComponent("SplashScreen", () => HOC(SplashScreen), () => SplashScreen);
     Navigation.registerComponent("MenuContent", () => HOC(MenuContent), () => MenuContent);
     Navigation.registerComponent("Login", () => HOC(Login), () => Login);
     Navigation.registerComponent("SelectLanguage", () => HOC(SelectLanguage), () => SelectLanguage);
     Navigation.registerComponent("ContactUs", () => HOC(ContactUs), () => ContactUs);
     Navigation.registerComponent("Notifications", () => HOC(Notifications), () => Notifications);
     Navigation.registerComponent("Register", () => HOC(Register), () => Register);
     Navigation.registerComponent("UpdateMyProfile", () => HOC(UpdateMyProfile), () => UpdateMyProfile);
     Navigation.registerComponent("Home", () => HOC(Home), () => Home);
     Navigation.registerComponent("AboutElkabir", () => HOC(AboutElkabir), () => AboutElkabir);
     Navigation.registerComponent("AbcTeam", () => HOC(AbcTeam), () => AbcTeam);
     Navigation.registerComponent("AboutAbc", () => HOC(AboutAbc), () => AboutAbc);
     Navigation.registerComponent("MdtTeam", () => HOC(MdtTeam), () => MdtTeam);
     Navigation.registerComponent("MdtPreparatoryClinc", () => HOC(MdtPreparatoryClinc), () => MdtPreparatoryClinc);
     Navigation.registerComponent("OncolplasticBreastReconstructionClinc", () => HOC(OncolplasticBreastReconstructionClinc), () => OncolplasticBreastReconstructionClinc);
     Navigation.registerComponent("BreastImagingTeam", () => HOC(BreastImagingTeam), () => BreastImagingTeam);
     Navigation.registerComponent("ChemoTeam", () => HOC(ChemoTeam), () => ChemoTeam);
     Navigation.registerComponent("Laboratories", () => HOC(Laboratories), () => Laboratories);
    
}

