import React,{Component} from 'react';
import AsyncStorage  from '@react-native-community/async-storage'
import {View,Image,Text,TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';
import {Button,Icon,Radio} from 'native-base';
import Strings from '../assets/strings';
import  {changeLanguage,changeColor} from '../actions/LanguageActions';
import {  moderateScale, responsiveFontSize, responsiveWidth,responsiveHeight } from "../utils/responsiveDimensions";
import {removeItem} from '../actions/MenuActions';
import Checkbox from 'react-native-custom-checkbox';
import {enableSideMenu,pop} from '../controlls/NavigationControll'
import {arrabicFont,englishFont} from '../common/AppFont'
import * as colors from '../assets/colors'
import * as Animatable from 'react-native-animatable';
import { ScrollView } from 'react-native-gesture-handler';
import AppHomeHeader from '../components/AppHomeHeader'
import Menu from '../components/Menu'

class SelectLanguage extends Component {



    state={
        showModal: false,
        lang: this.props.isRTL
    }
    
    componentDidMount(){
        enableSideMenu(true,this.props.isRTL)
    }
  
    componentWillUnmount(){
        this.props.removeItem()
      }

      componentDidUpdate(){
        enableSideMenu(false, null)
      }

      settingsItem = (item) =>{
        const {isRTL,barColor} = this.props
        return(
            <View style={{width:responsiveWidth(90),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between',marginTop:moderateScale(10)}}>
                <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'white',fontSize:responsiveFontSize(3)}} >{item}</Text>
                <Text style={{fontFamily:isRTL?arrabicFont:englishFont, color:'white',fontSize:responsiveFontSize(3)}} >{item}</Text>
            </View>
        )
      }

      content = () => {
        const {isRTL,barColor} = this.props
        const {showModal,lang} = this.state;
         return(
           <View style={{ marginTop:moderateScale(0)}}>
              <TouchableOpacity
                activeOpacity={1}
                 onPress={()=>{this.setState({showModal:!showModal})}}
                 style={{backgroundColor:'white', marginTop:moderateScale(10), height:responsiveHeight(9),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between', width:responsiveWidth(90),alignSelf:'center',borderRadius:moderateScale(2),borderColor:'white',elevation:1,shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1}}>
                    <Text style={{fontFamily:isRTL?arrabicFont:englishFont,color:'#C4C4C4',fontSize:responsiveFontSize(6.5),marginHorizontal:moderateScale(4)}} >{lang?'اللغة العربية':'English'}</Text>
                    <View style={{marginHorizontal:moderateScale(4)}}>
                        <Icon name={showModal?'chevron-small-up':'chevron-small-down'} type='Entypo'  style={{color:'#C4C4C4',fontSize:responsiveFontSize(6)}} />
                    </View>
                </TouchableOpacity>
               
                {showModal&&
                <Animatable.View animation='flipInX' duration={300}  style={{marginTop:moderateScale(1), shadowOffset: { height: 2,width:0 }, shadowColor: 'black',shadowOpacity: 0.1,height:responsiveHeight(22),backgroundColor:'white', alignItems:'center', width:responsiveWidth(90),alignSelf:'center',borderRadius:moderateScale(2),elevation:1,}}>
                
                <TouchableOpacity
                 onPress={()=>{ this.setState({lang:false,showModal:false}) }}
                 style={{borderBottomWidth:0.3,borderBottomColor:'#DBDBDB',backgroundColor:'white', marginTop:moderateScale(4), height:responsiveHeight(9),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between', width:responsiveWidth(90)}}>
                    <Text style={{fontFamily:englishFont, color:lang?'#C4C4C4':'black',fontSize:responsiveFontSize(6.5),marginHorizontal:moderateScale(4)}} >English</Text>
                    {!lang&&
                    <View style={{marginHorizontal:moderateScale(4)}}>
                    <Checkbox            
                        checked={lang?false:true}
                        style={{marginHorizontal:moderateScale(4), borderColor:colors.darkBlue, backgroundColor: colors.darkBlue,color: 'white',borderRadius: 0}}
                        /*onChange={(name, checked) => {
                        this.setState({ agreeTerms: checked });
                        }}*/
                    />
                    </View>
                    }
                </TouchableOpacity>

                <TouchableOpacity 
                onPress={()=>{ this.setState({lang:true,showModal:false}) }}
                style={{backgroundColor:'white', marginVertical:moderateScale(4), height:responsiveHeight(9),alignItems:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between', width:responsiveWidth(90)}}>
                    <Text style={{fontFamily:arrabicFont, color:lang?'black':'#C4C4C4',fontSize:responsiveFontSize(6.5),marginHorizontal:moderateScale(4)}} >اللغة العربية</Text>
                    {lang&&
                     <View style={{marginHorizontal:moderateScale(4)}}>
                     <Checkbox               
                         checked={lang?true:false}
                         style={{marginHorizontal:moderateScale(4), borderColor:colors.darkBlue, backgroundColor:colors.darkBlue,color: 'white',borderRadius: 0}}
                         /*onChange={(name, checked) => {
                         this.setState({ agreeTerms: checked });
                         }}*/
                     />
                     </View>
                    }
                </TouchableOpacity>
                
                </Animatable.View>
                }
            
                <TouchableOpacity
                    style={{width:responsiveWidth(50),alignSelf:'center',height:responsiveHeight(7),alignItems:'center',justifyContent:'center',backgroundColor:colors.darkBlue,marginTop:moderateScale(15),borderRadius:moderateScale(10)}}
                    onPress={()=>{
                    if(lang){
                        this.props.changeLanguage(true);
                        Strings.setLanguage('ar');
                        AsyncStorage.setItem('@lang','ar')
                        console.log('lang   '+ this.props.isRTL)
                        pop()
                    }else{
                        this.props.changeLanguage(false);
                        Strings.setLanguage('en');
                        AsyncStorage.setItem('@lang','en')
                        console.log('lang   '+ this.props.isRTL)
                        pop()
                    }
                    }}
                    >
                    <Text style={{fontFamily:isRTL?arrabicFont:englishFont,color:'white',fontSize:responsiveFontSize(6)}}>{Strings.done}</Text>
                </TouchableOpacity>
           
          </View>
         )
       }
 
     
    render(){
        const {isRTL,barColor} = this.props
        const {showModal,lang} = this.state;
        return(
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                <AppHomeHeader />
                {this.content()}
                {this.props.showMenuModal&&<Menu/>}
            </View>
        )
    }
}



const mapToStateProps = state => ({
    isRTL: state.lang.RTL, 
    barColor: state.lang.color ,
    showMenuModal:state.menu.showMenuModal
   
})

const mapDispatchToProps = {
    changeLanguage,
    changeColor,
    removeItem
}



export default connect(mapToStateProps,mapDispatchToProps)(SelectLanguage);