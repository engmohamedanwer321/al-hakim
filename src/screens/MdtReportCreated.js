import React, { Component } from 'react';
import {
    View,FlatList,RefreshControl,ActivityIndicator,Easing, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppHomeHeader from '../components/AppHomeHeader'
import Menu from '../components/Menu'
import ApllicationCard from '../components/ApllicationCard'
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Spiner from '../common/Spiner'

class MdtReportCreated extends Component {

  
    state= {
        loading:false,
    }

    applicationStatusChannge = () =>{
        this.setState({loading:true})
        var data={
            status:'MDTR-Review',
            'id':this.props.data.id
        }
        axios.put(`${BASE_END_POINT}applications`,data,{
            headers: {
                "Content-Type": 'application/json',
                "Authorization": `authorization ${this.props.currentUser.token}`
            }
        })
        .then(response=>{
            console.log('change application status   ',response)
            this.setState({loading:false})
            push('ShowFinalRoport',this.props.data)
        })
        .catch(error=>{
            this.setState({loading:false})
            console.log('error   ',error.response)
            console.log('error2   ',error)
        })
    }

    nextButton = () => {
        const { isRTL } = this.props
        return (
            <TouchableOpacity
            onPress={() => {
               this.applicationStatusChannge()
             }}
             style={{marginTop:moderateScale(10), flexDirection:isRTL?'row-reverse':'row', alignSelf: 'center', marginVertical: moderateScale(15), height: responsiveHeight(6), width: wp(60), justifyContent: 'center', alignItems: 'center', backgroundColor: '#946AB3', borderRadius: moderateScale(8) }} >
                <Text style={{fontSize:responsiveFontSize(6), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.next}</Text>
            </TouchableOpacity>
        )
    }

    render() {
        const {isRTL} = this.props
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                <AppHomeHeader />
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    
                     
                     <FastImage
                     style={{width:wp(60),height:responsiveHeight(30),}}
                     resizeMode='contain'
                     source={require('../assets/imgs/report.png')}
                     />
                   
                    <Text style={{width:wp(60),textAlign:'center', marginTop:moderateScale(-5),fontSize:responsiveFontSize(9), color:'#707070', fontFamily:boldFont}}>{Strings.mdtr}</Text>
                    {this.nextButton()}
                </View>
                {this.state.loading &&
                    <LoadingDialogOverlay title={Strings.wait} />
                }
                {this.props.showMenuModal&&<Menu/>}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(MdtReportCreated);

