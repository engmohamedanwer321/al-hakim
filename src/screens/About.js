import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppCommanHeader from '../components/AppCommanHeader'
import Menu from '../components/Menu'

class About extends Component {

  

    componentDidMount() {
        enableSideMenu(false, null)
    }

    link = (text,page) =>{
        const { isRTL } = this.props
        return(
            <TouchableOpacity
            onPress={()=>{
                if(page){
                    push(page)
                }
            }}
            style={{width:wp(80),alignSelf:'center'}} 
            >
                <Text style={{alignSelf:isRTL?'flex-end':'flex-start',textDecorationLine:'underline', marginTop:moderateScale(10),fontSize:responsiveFontSize(8), color:'white', fontFamily:isRTL?arrabicFont:englishFont }}>
                   {text}
                </Text>
            </TouchableOpacity>
        )
    }
   

    
    registerButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => { /*this.login()*/ }} style={{flexDirection:isRTL?'row-reverse':'row', alignSelf: 'center', marginTop: moderateScale(15), height: responsiveHeight(7), width: wp(80), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.white, borderRadius: moderateScale(5) }} >
                <Icon name='square-edit-outline' type='MaterialCommunityIcons' style={{color:colors.lightBlue,fontSize:responsiveFontSize(8)}} />
                <Text style={{fontSize:responsiveFontSize(8),marginHorizontal:moderateScale(2),  color: colors.lightBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.save}</Text>
            </TouchableOpacity>
        )
    }


  


    render() {
        const {isRTL} = this.props
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                
                <AppCommanHeader title={Strings.about} />
          
                <ScrollView showsVerticalScrollIndicator={false}>
                    
                    <View style={{alignSelf:isRTL?"flex-end":'flex-start',marginTop:moderateScale(5), marginHorizontal:moderateScale(6),flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'center'}}>
                        <FastImage
                            source={require('../assets/imgs/profileicon.jpg')}
                            style={{borderWidth:3,borderColor:'#946AB3',width:30,height:30,borderRadius:15}}
                        />
                        <Text style={{fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(2),  color: colors.darkBlue, fontFamily: boldFont }}>Elkhabir</Text>
                    </View>

                   

                    <View style={{minHeight:responsiveHeight(65), padding:moderateScale(10),paddingBottom:moderateScale(40), backgroundColor:"#946AB3",width:wp(90),borderRadius:moderateScale(6),alignSelf:'center',marginTop:moderateScale(5),marginBottom:moderateScale(20)}} >
                       
                        {this.link(Strings.aboutElkhabir,'AboutElkabir')}
                        {this.link(Strings.aboutABC,'AboutAbc')}
                       
                    </View>

                </ScrollView>



                {/*this.props.showMenuModal&&<Menu/>*/}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(About);

