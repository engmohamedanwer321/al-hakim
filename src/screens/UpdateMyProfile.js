import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView,FlatList, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import RBSheet from "react-native-raw-bottom-sheet";
//import DateTimePicker from '@react-native-community/datetimepicker';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import {countries} from '../assets/dumyData' 
import {isEmail} from '../controlls/Validations'
import moment from 'moment'

nationalities = ["egypt","ksa","oman","usa",'barazil','bolanda','engeland']

class UpdateMyProfile extends Component {

    constructor(props){
        super(props)
        moment.locale('en')
    }

    state = {
        firstName:this.props.currentUser.first_name,
        lastName:this.props.currentUser.last_name,
        gender:this.props.currentUser.gender,
        phone:this.props.currentUser.phone.toString(),
        nationality:this.props.currentUser.nationality,
        ssn:this.props.currentUser.ssn,
        countries:[],
        email:this.props.currentUser.email,
        password: ' ',
        id:' ',
        
        showCalender:false,
        date:new Date(),
        birthDate:this.props.currentUser.date_of_birth, //moment(this.props.currentUser.date_of_birth).format("DD-MM-YYYY"),

        loading: false,
    }

    componentDidMount() {
        enableSideMenu(false, null)
        console.log("USER   ",this.props.currentUser)
        const result = Object.keys(countries).map(i => countries[i])
        console.log(" result   ",result)
        this.setState({countries:result.sort()})
    }

    title = () => {
        const { isRTL } = this.props
        return (
            <Text style={{alignSelf:'center',marginTop:moderateScale(5), fontSize:responsiveFontSize(25),  color: 'white', fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.editProfile}</Text>
        )
    }

    backButton = () => {
        const { isRTL } = this.props
        return (
            <TouchableOpacity 
            onPress={()=>{
                pop()
            }}
            style={{alignSelf:isRTL?'flex-end':'flex-start', marginTop:moderateScale(13),marginHorizontal:moderateScale(10)}}>
                <Icon name={isRTL?'arrowright':'arrowleft'} type='AntDesign' style={{fontSize:responsiveFontSize(20),color:'white'}} />
            </TouchableOpacity>
        )
    }


    firstNameInput = () => {
        const { isRTL } = this.props
        const { firstName } = this.state
        return (
            <View style={{marginTop: moderateScale(15), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.firstName}</Label>    
                    <Input
                        value={firstName}
                        onChangeText={(val) => { this.setState({ firstName: val }) }}
                        style={{color:'white', fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.firstName}
                    />
                </Item>

                {firstName.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    
    lastNameInput = () => {
        const { isRTL } = this.props
        const { lastName } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.lastName}</Label>    
                    <Input
                        value={lastName}
                        onChangeText={(val) => { this.setState({ lastName: val }) }}
                        style={{color:'white', fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.lastName}
                    />
                </Item>

                {lastName.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    phoneInput = () => {
        const { isRTL } = this.props
        const { phone } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.phone}</Label>    
                    <Input
                        value={""+phone}
                        onChangeText={(val) => { this.setState({ phone: val }) }}
                        style={{color:'white', fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.phone}
                    />
                </Item>

                {phone.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    nationnalityInput = () => {
        const { isRTL } = this.props
        const { nationality } = this.state
        return (
            <View style={{marginTop: moderateScale(6), width: wp(80), alignSelf: 'center', }}>
                {nationality&&
                    <Text style={{fontSize:15, color:colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}} >{Strings.nationality}</Text>
                 }
                <TouchableOpacity 
                onPress={()=>{
                    this.RBSheet.open()
                }}
                style={{ width:wp(80),height:responsiveHeight(6),borderBottomColor:colors.darkGray,borderBottomWidth:0.5,justifyContent:'flex-end'}} >
                    <Text style={{fontSize:nationality?responsiveFontSize(5):17, color:nationality?'white':colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}} >{nationality?nationality:Strings.nationality}</Text>
                </TouchableOpacity>
                {nationality==false&&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    genderInput = () => {
        const { isRTL } = this.props
        const { gender } = this.state
        return (
            <View style={{marginTop: moderateScale(6), width: wp(80), alignSelf: 'center', }}>
                {gender&&
                    <Text style={{fontSize:15, color:colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}} >{Strings.gender}</Text>
                 }
                <TouchableOpacity 
                onPress={()=>{
                    this.genderSheetRef.open()
                }}
                style={{ width:wp(80),height:responsiveHeight(6),borderBottomColor:colors.darkGray,borderBottomWidth:0.5,justifyContent:'flex-end'}} >
                    <Text style={{fontSize:gender?responsiveFontSize(5):17, color:gender?'white':colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}} >{gender?gender:Strings.gender}</Text>
                </TouchableOpacity>
                {gender==false&&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    birthDateInput = () => {
        const { isRTL } = this.props
        const { birthDate } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                 {birthDate&&
                    <Text style={{fontSize:15, color:colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}} >{Strings.birthDate}</Text>
                 }
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({showCalender:true})
                }}
                style={{ width:wp(80),height:responsiveHeight(6),borderBottomColor:colors.darkGray,borderBottomWidth:0.5,justifyContent:'flex-end'}} >
                    <Text style={{fontSize:birthDate?responsiveFontSize(5):17, color:birthDate?'white':colors.darkGray,alignSelf:isRTL?'flex-end':'flex-start'}} >{birthDate?birthDate:Strings.birthDate}</Text>
                </TouchableOpacity>
                {birthDate==false&&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    idInput = () => {
        const { isRTL } = this.props
        const { id } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.id}</Label>    
                    <Input
                        keyboardType='phone-pad'
                        onChangeText={(val) => { this.setState({ id: val }) }}
                        style={{color:'white',fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.id}
                    />
                </Item>

                {id.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    emailInput = () => {
        const { isRTL } = this.props
        const { email } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.email}</Label>    
                    <Input
                        keyboardType='email-address'
                        onChangeText={(val) => { 
                            this.setState({ email: val }) 
                            if(val){
                                this.setState({emailError:null}) 
                             }else{
                                 this.setState({emailError:Strings.require}) 
                             }
                        }}
                        editable={false}
                        value={email}
                        style={{color:'white',fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.email}
                    />
                </Item>

                {email.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    passwordInput = () => {
        const { isRTL } = this.props
        const { password, hidePassword } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
               <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(80)}}>

                    <Label style={{color:colors.darkGray}} >{Strings.password}</Label>    
                    <Input
                        secureTextEntry
                        onChangeText={(val) => { this.setState({ password: val }) }}
                        style={{color:'white',fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.password}
                    />
                </Item>
               
               
                {password.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    ssnInput = () => {
        const { isRTL } = this.props
        const { ssn,nationality } = this.state
        return (
            <View style={{ marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
               <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(80)}}>

                    <Label style={{color:colors.darkGray}} >{nationality=='Egypt'?Strings.nationalityId:Strings.passportId}</Label>    
                    <Input
                       value={ssn}
                        onChangeText={(val) => { this.setState({ ssn: val }) }}
                        style={{color:'white', fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={nationality=='Egypt'?Strings.nationalityId:Strings.passportId}
                    />
                </Item>
               
               
                {ssn.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    getUserByToken = (token) => {
        axios.get(`${BASE_END_POINT}users/get_user`, {
            headers: {
                "Authorization": `authorization ${token}`
            }
        })
        .then(response => {
            this.setState({ loading: false })
            console.log("getUserByToken   ",response.data)
            const user ={...response.data,token:token}
            this.props.setUser(user)
            AsyncStorage.setItem('USER', JSON.stringify(user))
            resetTo('Home')
        })
        .catch(error=>{
            this.setState({ loading: false })
            console.log("error  ",error)
        })
    }

    update = () => {
        const {ssn,firstName,lastName,phone,gender,nationality, birthDate,password,email } = this.state
        if (!firstName.replace(/\s/g, '').length) {
            this.setState({ firstName: '' })
        }
        if (!ssn.replace(/\s/g, '').length) {
            this.setState({ ssn: '' })
        }
        if (!lastName.replace(/\s/g, '').length) {
            this.setState({ lastName: '' })
        }
        if(!isEmail(email)){
            this.setState({emailError:Strings.emailFormatInvalid})
        }
        if (!email.replace(/\s/g, '').length) {
            this.setState({ email: '' })
        }
        if (!phone.replace(/\s/g, '').length) {
            this.setState({ phone: '' })
        }
        if (!password.replace(/\s/g, '').length) {
            this.setState({ password: '' })
        }
        if (!gender) {
            this.setState({ gender:false })
        }

        if (!nationality) {
            this.setState({ nationality:false })
        }
       
        if (!birthDate) {
            this.setState({ birthDate:false })
        }

        if(nationality=='Egypt'){
            if(ssn.length!=14){
                RNToasty.Error({title:Strings.nationalityMustBe14Digits})
            }else{
                if (
                    firstName.replace(/\s/g, '').length && lastName.replace(/\s/g, '').length && email.replace(/\s/g, '').length &&isEmail(email)&&
                    phone.replace(/\s/g, '').length && password.replace(/\s/g, '').length && gender && nationality && birthDate
                    ) {
                        this.setState({ loading: true })
                        var data= new URLSearchParams()
                        data.append('first_name',firstName)
                        data.append('last_name',lastName)
                        data.append('email',email)
                        data.append('phone',phone)
                        data.append('nationality',nationality)
                        data.append('ssn',ssn)
                        data.append('gender',gender)
                        data.append('role','patient')
                        data.append('date_of_birth',birthDate)
                        data.append('password',password)
                        axios.put(`${BASE_END_POINT}users`, data, {
                            headers: {
                                "Content-Type": 'application/x-www-form-urlencoded',
                                "Authorization": `authorization ${this.props.currentUser.token}`
                            }
                        })
                        .then(response => {
                            //this.setState({ loading: false })
                            console.log("Updage done  ",response.data)
                            this.getUserByToken(this.props.currentUser.token)
                        })
                        .catch(error => {
                            this.setState({ loading: false })
                            console.log("ERROR   ",error.response)
                            if (error.response.status == 401) {
                                RNToasty.Error({ title: strings.loginUserDataIncorrect })
                            }
                        })
                    }
             
            }
        }else{
            if (
                firstName.replace(/\s/g, '').length && lastName.replace(/\s/g, '').length && email.replace(/\s/g, '').length &&isEmail(email)&&
                phone.replace(/\s/g, '').length && password.replace(/\s/g, '').length && gender && nationality && birthDate
                ) {
                    this.setState({ loading: true })
                    var data= new URLSearchParams()
                    data.append('first_name',firstName)
                    data.append('last_name',lastName)
                    data.append('email',email)
                    data.append('phone',phone)
                    data.append('nationality',nationality)
                    data.append('ssn',ssn)
                    data.append('gender',gender)
                    data.append('role','patient')
                    data.append('date_of_birth',birthDate)
                    data.append('password',password)
                    axios.put(`${BASE_END_POINT}users`, data, {
                        headers: {
                            "Content-Type": 'application/x-www-form-urlencoded',
                            "Authorization": `authorization ${this.props.currentUser.token}`
                        }
                    })
                    .then(response => {
                        //this.setState({ loading: false })
                        console.log("Updage done  ",response.data)
                        this.getUserByToken(this.props.currentUser.token)
                    })
                    .catch(error => {
                        this.setState({ loading: false })
                        console.log("ERROR   ",error.response)
                        if (error.response.status == 401) {
                            RNToasty.Error({ title: strings.loginUserDataIncorrect })
                        }
                    })
                }
        
        }
       
    }

    registerButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => { 
                this.update()
             }} style={{flexDirection:isRTL?'row-reverse':'row', alignSelf: 'center', marginVertical: moderateScale(15), height: responsiveHeight(7), width: wp(80), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.white, borderRadius: moderateScale(5) }} >
                <Icon name='square-edit-outline' type='MaterialCommunityIcons' style={{color:colors.lightBlue,fontSize:responsiveFontSize(15)}} />
                <Text style={{fontSize:responsiveFontSize(8),marginHorizontal:moderateScale(2),  color: colors.lightBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.save}</Text>
            </TouchableOpacity>
        )
    }

    nationalitySheet = () =>{
        const { isRTL } = this.props
        const { email } = this.state
        return (
            <RBSheet
            ref={ref => {
              this.RBSheet = ref;
            }}
            height={responsiveHeight(40)}
            duration={0}
            customStyles={{
              container: {
                justifyContent: "center",
                alignItems: "center",
                borderTopLeftRadius:moderateScale(5),
                borderTopRightRadius:moderateScale(5)
              }
            }}
          >
             <FlatList
            showsVerticalScrollIndicator={false}
            data={this.state.countries}
            renderItem={({item})=>(
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({nationality:item})
                    this.RBSheet.close()
                }}
                style={{width:wp(96),paddingVertical:moderateScale(6),borderBottomColor:colors.darkGray,borderBottomWidth:0.5}}>
                    <Text style={{color:colors.darkGray, fontFamily:englishFont,fontSize:responsiveFontSize(7),marginHorizontal:moderateScale(6)}}>{item}</Text>
                </TouchableOpacity>
            )}
            />
            
          </RBSheet>
        )
    }

    genderSheet = () =>{
        const { isRTL } = this.props
        const { email } = this.state
        return (
            <RBSheet
            ref={ref => {
              this.genderSheetRef = ref;
            }}
            height={responsiveHeight(40)}
            duration={0}
            customStyles={{
              container: {
                //justifyContent: "center",
                alignItems: "center",
                borderTopLeftRadius:moderateScale(5),
                borderTopRightRadius:moderateScale(5)
              }
            }}
          >
            
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({gender:'femail'})
                    this.genderSheetRef.close()
                }}
                style={{width:wp(96),paddingVertical:moderateScale(6),borderBottomColor:colors.darkGray,borderBottomWidth:0.5}}>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start', color:colors.darkGray, fontFamily:englishFont,fontSize:responsiveFontSize(7),marginHorizontal:moderateScale(6)}}>{Strings.feMale}</Text>
                </TouchableOpacity>

                <TouchableOpacity 
                onPress={()=>{
                    this.setState({gender:'male'})
                    this.genderSheetRef.close()
                }}
                style={{width:wp(96),paddingVertical:moderateScale(6),borderBottomColor:colors.darkGray,borderBottomWidth:0.5}}>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start',color:colors.darkGray, fontFamily:englishFont,fontSize:responsiveFontSize(7),marginHorizontal:moderateScale(6)}}>{Strings.male}</Text>
                </TouchableOpacity>

          </RBSheet>
        )
    }


    calender = () => {
        const {date} = this.state
        return(
            <DateTimePickerModal
            value={date}
            maximumDate={new Date()}
            is24Hour={true}
            isVisible={this.state.showCalender}
            mode='date'
            onConfirm={(date)=>{
                console.log("DATE   ",date)
                const d = moment(date).format('DD-MM-YYYY')
                console.log("DAT   ",d)
                this.setState({showCalender:false, birthDate:d,date:date})
            }}
            onCancel={()=>{
                this.setState({showCalender:false})
            }}
            />
        )
    }


    render() {
        const { loading, showCalender ,nationality} = this.state
        return (
            <View style={{backgroundColor:colors.blueBackground,flex:1}}>
                    {this.backButton()}
                    
                    <ScrollView showsVerticalScrollIndicator={false}>
                    {this.title()}
                    {this.firstNameInput()}
                    {this.lastNameInput()}
                    {this.nationnalityInput()}
                    {nationality&&this.ssnInput()}
                    {this.emailInput()}
                    {this.phoneInput()}
                    {this.birthDateInput()}
                    {this.genderInput()}
                    {this.passwordInput()}
                    {this.registerButton()}
                    {this.nationalitySheet()}
                    {this.genderSheet()}
                    {showCalender&&this.calender()}
                    </ScrollView>
                
                {loading &&
                    <LoadingDialogOverlay title={Strings.wait} />
                }

            

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    currentUser:state.auth.currentUser, 
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(UpdateMyProfile);

