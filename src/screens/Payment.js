import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppHomeHeader from '../components/AppHomeHeader'
import Menu from '../components/Menu'

class Payment extends Component {
    state={
        fullName:' ',
        cardNumber:' ',
        expireDate:' ',
        securityCode:' ',
    }

    componentDidMount() {
        enableSideMenu(false, null)
    }

    paymentButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => { push('DetermineApplicationContactTime') /*this.login()*/ }} style={{flexDirection:isRTL?'row-reverse':'row', alignSelf: 'center', marginTop: moderateScale(20), height: responsiveHeight(8), width: wp(100), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue}} >
                <Icon name='checkcircleo' type='AntDesign' style={{color:colors.white,fontSize:responsiveFontSize(8)}} />
                <Text style={{fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(2),  color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.confirmAndPay}</Text>
            </TouchableOpacity>
        )
    }

    paymentHeader = () => {
        const { isRTL } = this.props
        return (
           <View style={{width:wp(100),backgroundColor:colors.darkBlue,paddingVertical:moderateScale(10),marginTop:moderateScale(5)}}>
               <View style={{width:wp(90),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between'}}>
                    <Text style={{fontSize:responsiveFontSize(5), color:'white', fontFamily:boldFont}}>{Strings.check2} </Text>
                    <View>
                        <Text style={{alignSelf:isRTL?'flex-start':'flex-end', fontSize:responsiveFontSize(5), color:'white', fontFamily:boldFont}}>45$</Text>
                        <Text style={{alignSelf:isRTL?'flex-start':'flex-end', fontSize:responsiveFontSize(5), color:colors.darkGray, fontFamily:englishFont}}>30 min</Text>
                    </View>
               </View>
               <View style={{width:wp(90),alignSelf:'center'}}>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start', fontSize:responsiveFontSize(5), color:colors.darkGray, fontFamily:englishFont}}>{`${new Date()}`} </Text>
               </View>
               
           </View>
        )
    }

    cardHeader = () => {
        const { isRTL } = this.props
        return (
           <View style={{borderBottomColor:colors.darkGray,borderBottomWidth:0.5, width:wp(100),backgroundColor:'white',paddingVertical:moderateScale(10)}}>
               <View style={{ width:wp(90),alignSelf:'center',flexDirection:isRTL?'row-reverse':'row',justifyContent:'space-between'}}>
                    <View style={{flexDirection:isRTL?'row-reverse':'row'}}>
                        <FastImage
                        //resizeMode='contain'
                        source={require('../assets/imgs/paymentIcon.png')}
                        style={{width:wp(12),height:responsiveHeight(5)}}
                        />
                        <View>
                            <Text style={{alignSelf:isRTL?'flex-end':'flex-start', fontSize:responsiveFontSize(5), color:colors.lightBlue, fontFamily:englishFont}}>Saved Card</Text>
                            <Text style={{alignSelf:isRTL?'flex-end':'flex-start', fontSize:responsiveFontSize(5), color:'black', fontFamily:englishFont}}>{'\u2B24'}{'\u2B24'}{'\u2B24'}{'\u2B24'} {'\u2B24'}{'\u2B24'}{'\u2B24'}{'\u2B24'} {'\u2B24'}{'\u2B24'}{'\u2B24'}{'\u2B24'} 1811</Text>
                        </View>
                    </View>
                    <Icon name={isRTL?'left':'right'} type='AntDesign' style={{color:'black',fontSize:responsiveFontSize(7)}} />
               </View>
               
           </View>
        )
    }


    fullNameInput = () => {
        const { isRTL } = this.props
        const { fullName } = this.state
        return (
            <View style={{marginTop: moderateScale(10), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{  borderBottomColor:colors.darkGray,borderBottomWidth:0.5,  flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.firstName}</Label>    
                    <Input
                        onChangeText={(val) => { this.setState({ fullName: val }) }}
                        style={{color:colors.lightBlue, fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.firstName}
                    />
                </Item>

                {fullName.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    cardNumberInput = () => {
        const { isRTL } = this.props
        const { cardNumber } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{  borderBottomColor:colors.darkGray,borderBottomWidth:0.5,  flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.cardNumber}</Label>    
                    <Input
                        onChangeText={(val) => { this.setState({ cardNumber: val }) }}
                        style={{color:colors.lightBlue, fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.cardNumber}
                    />
                </Item>

                {cardNumber.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    expireDateInput = () => {
        const { isRTL } = this.props
        const { expireDate } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{  borderBottomColor:colors.darkGray,borderBottomWidth:0.5,  flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.expiryDate}</Label>    
                    <Input
                        onChangeText={(val) => { this.setState({ expireDate: val }) }}
                        style={{color:colors.lightBlue, fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.expiryDate}
                    />
                </Item>

                {expireDate.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    securityCodeInput = () => {
        const { isRTL } = this.props
        const { securityCode } = this.state
        return (
            <View style={{marginTop: moderateScale(5), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{  borderBottomColor:colors.darkGray,borderBottomWidth:0.5,  flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.securityCode}</Label>    
                    <Input
                        onChangeText={(val) => { this.setState({ securityCode: val }) }}
                        style={{color:colors.lightBlue, fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.securityCode}
                    />
                </Item>

                {securityCode.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


  


    render() {
        const {isRTL} = this.props
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                <AppHomeHeader />
                <ScrollView showsVerticalScrollIndicator={false}>
                    <Text style={{marginTop:moderateScale(5), alignSelf:'center', fontSize:responsiveFontSize(9), color:'black', fontFamily:englishFont}}>{Strings.paymentOptions} </Text>
                    {this.paymentHeader()}
                    {this.cardHeader()}
                    <View style={{width:wp(100),backgroundColor:'white'}}>
                    {this.fullNameInput()}
                    {this.cardNumberInput()}
                    {this.expireDateInput()}
                    {this.securityCodeInput()}
                    {this.paymentButton()}
                    </View>
                </ScrollView>
                {this.props.showMenuModal&&<Menu/>}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(Payment);

