import React, { Component } from 'react';
import {
    View,FlatList,RefreshControl,ActivityIndicator, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppHomeHeader from '../components/AppHomeHeader'
import Menu from '../components/Menu'
import Checkbox from 'react-native-custom-checkbox';
//import DateTimePicker from '@react-native-community/datetimepicker';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import DocumentPicker from 'react-native-document-picker';
import moment from 'moment'
import ImagePicker from 'react-native-image-crop-picker';


class UploadReports extends Component {
    state={
        page:1,
        showMediaType:false,
        mediaType:null, //for check
        selectedMeida:null, //for box title
        media:[], //for ulpoad
        allMedia:[],//for show all media
        loading:false,
        reportId:0,
        reportName:' ',
    }

    createReport = () =>{
        const {currentUser} = this.props
        const {reportName} = this.state
     
            this.setState({loading:true})
            const data = new URLSearchParams()
            data.append('name',this.state.reportName)
            data.append('userId',this.props.currentUser.id)
            axios.post(`${BASE_END_POINT}reports`,data,{
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded',
                    "Authorization": `authorization ${this.props.currentUser.token}`
                }
            })
            .then(response=>{
                //RNToasty.Success({title:Strings.yourReportCreatedSuccessfuly})
                console.log('APP CRATED   ',response)
                this.uploadReport(response.data.data.id)
                //this.setState({reportId:response.data.data.id,page:2,loading:false})
            })
            .catch(error=>{
                this.setState({loading:false})
                console.log('error   ',error)
            })
        
    }

    uploadReport = (id) =>{
        this.setState({loading:true})
        console.log('id   ',this.state.reportId)
        const data = new FormData()
        data.append('reportId',id)
        this.state.media.map(file=>{
            data.append('document',file)    
        })
        
        axios.post(`${BASE_END_POINT}files/upload`,data,{
            headers: {
                "Content-Type": '..',
                "Authorization": `authorization ${this.props.currentUser.token}`
            }
        })
        .then(response=>{
            RNToasty.Success({title:Strings.youFilesUploadedSuccessfuly})
            console.log('File Uploades   ',response)
            //console.log('id   ',this.state.apllicationId)
            this.setState({loading:false, allMedia:[...this.state.allMedia,...this.state.media], media:[]}) 
        })
        .catch(error=>{
            this.setState({loading:false})
            console.log('error in file upload   ',error.response)
        })
    }


    nameInput = () => {
        const { isRTL } = this.props
        const { reportName } = this.state
        return (
            <View style={{marginTop: moderateScale(30), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{  borderBottomColor:colors.darkGray,borderBottomWidth:0.5,  flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.fileName}</Label>    
                    <Input
                        onChangeText={(val) => { this.setState({ reportName: val }) }}
                        style={{fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.fileName}
                    />
                </Item>

                {reportName.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    page1=()=>{
        const {isRTL,currentUser} = this.props
        const {reportName} = this.state
        return (
            <View  style={{flex:1,backgroundColor:'transparent',}}>
                {this.nameInput()} 
                <TouchableOpacity 
                onPress={()=>{
                    if (!reportName.replace(/\s/g, '').length) {
                        this.setState({ reportName: '' })
                    }else{
                        this.setState({page:2})
                    }
            
                    
                }}
                style={{alignSelf:'center',marginTop:moderateScale(10), justifyContent:'center', borderRadius:moderateScale(2), width:wp(80),height:responsiveHeight(7),backgroundColor:'#946AB3', alignItems:'center'}}>
                 <Text style={{ marginHorizontal:moderateScale(2), fontSize:responsiveFontSize(7), color:'white', fontFamily:englishFont}}>{Strings.create} </Text>
            </TouchableOpacity>   
            </View>
        )
    }
  


    pickMedia = async () =>{
        try {
            const results = await DocumentPicker.pickMultiple({
              type: [DocumentPicker.types.allFiles], //[DocumentPicker.types.images,DocumentPicker.types.video,DocumentPicker.types.audio],
            });

            console.log("ME   ",results)
            this.setState({media:[...this.state.media,...results]})    
        } catch (err) {
            console.log("errror   ",err)
        }
    }

    pickMediaFromPicker = async  () =>{
        ImagePicker.openCamera({
            width: 800,
            height: 800,
            //cropping: true,
          }).then(image => {
            console.log(" camer img   ",image);
            const i = {
                name:'image_'+new Date().getTime()+'.png',
                type:image.mime,
                size:image.size,
                uri:image.path,
            }
            console.log("I   ",image)
            this.setState({media:[...this.state.media,i]}) 
          }).catch(error=>{
              console.log("Error   ",error)
          })
          ;
    }

   

    mediaTypeBox = ()=>{
        const {isRTL} = this.props
        const {showMediaType,mediaType,selectedMeida} = this.state
        return(
            <View style={{width:wp(80), alignSelf:'center',marginTop:moderateScale(5)}}>
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({showMediaType:!showMediaType})
                }}
                 style={{width:wp(80),height:responsiveHeight(6),borderRadius:moderateScale(6),borderWidth:2,borderColor:'#946AB3',justifyContent:'space-between',alignItems:'center',flexDirection:isRTL?'row-reverse':'row'}}>
                    {mediaType?
                    <Text style={{marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:'#946AB3', fontFamily:englishFont}}>{selectedMeida} </Text>
                    :
                    <Text style={{marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:'#946AB3', fontFamily:englishFont}}>{Strings.docType} </Text>
                    }
                    <View style={{marginHorizontal:moderateScale(3)}}>
                    <Icon name='caretdown' type='AntDesign' style={{color:'#946AB3',fontSize:responsiveFontSize(8)}} />
                    </View>
                </TouchableOpacity>
                
                {showMediaType&&
                <View style={{marginTop:moderateScale(2),width:wp(80),borderRadius:moderateScale(6),borderWidth:2,borderColor:'#946AB3'}}>
                    <TouchableOpacity 
                    onPress={()=>{
                        this.pickMedia()
                        this.setState({ showMediaType:false, selectedMeida:Strings.photo,mediaType:'photo'})
                    }}
                    style={{borderTopLeftRadius:moderateScale(6),borderTopRightRadius:moderateScale(6), justifyContent:'center',backgroundColor:mediaType=='photo'?'#F79EFE':'tranparent', flex:1,height:responsiveHeight(5),}}>
                        <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:mediaType=='photo'?'white':'#946AB3', fontFamily:englishFont}}>{Strings.photo} </Text>
                    </TouchableOpacity>

                    <TouchableOpacity 
                    onPress={()=>{
                       // this.pickMedia()
                       this.pickMediaFromPicker()
                        this.setState({showMediaType:false, selectedMeida:Strings.camera,mediaType:'camera'})
                    }}
                    style={{justifyContent:'center',backgroundColor:mediaType=='camera'?'#F79EFE':'tranparent', flex:1,height:responsiveHeight(5),}}>
                        <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:mediaType=='camera'?'white':'#946AB3', fontFamily:englishFont}}>{Strings.camera} </Text>
                    </TouchableOpacity>

                    <TouchableOpacity 
                    onPress={()=>{
                        this.pickMedia()
                        this.setState({showMediaType:false, selectedMeida:Strings.video,mediaType:'video'})
                    }}
                    style={{justifyContent:'center',backgroundColor:mediaType=='video'?'#F79EFE':'tranparent', flex:1,height:responsiveHeight(5),}}>
                        <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:mediaType=='video'?'white':'#946AB3', fontFamily:englishFont}}>{Strings.video} </Text>
                    </TouchableOpacity>

                    <TouchableOpacity 
                    onPress={()=>{
                        this.pickMedia()
                        this.setState({showMediaType:false, selectedMeida:Strings.voiceNote,mediaType:'voice'})
                    }}
                    style={{borderBottomLeftRadius:moderateScale(6),borderBottomRightRadius:moderateScale(6),justifyContent:'center', flex:1,backgroundColor:mediaType=='voice'?'#F79EFE':'tranparent',height:responsiveHeight(5),}}>
                        <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:mediaType=='voice'?'white':'#946AB3', fontFamily:englishFont}}>{Strings.voiceNote} </Text>
                    </TouchableOpacity>
                </View>
                }
            </View>
        )
    }

    mediaItem = (val,uploaded) =>{
        const {isRTL} = this.props
        const {media,allMedia} = this.state
        return(
            val.map((file,index)=>(
            <View style={{width:wp(80),alignSelf:'center',marginTop:moderateScale(5),flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                    <Icon name='attachment' type='Entypo' style={{color:colors.darkGray ,fontSize:responsiveFontSize(10)}} />
                    <Text style={{maxWidth:wp(60), marginHorizontal:moderateScale(5), fontSize:responsiveFontSize(7), color:colors.darkBlue, fontFamily:englishFont}}>{file.name}</Text>
                </View>
                
                {!uploaded&&
                <TouchableOpacity
                onPress={()=>{
                    var removedMedia = media.splice(index,1)
                    console.log("media    ",media)
                    this.setState({media:media})
                }}
                >
                    <Icon name='delete' type='MaterialCommunityIcons' style={{color:colors.darkGray ,fontSize:responsiveFontSize(12)}} />
                </TouchableOpacity>
                }       
            </View>
            ))
        )
    }

    uploadButton=()=>{
        const {isRTL} = this.props
        const {media,selectedDate,agreeDate} = this.state
        return (
            <TouchableOpacity 
            onPress={()=>{
              if(media.length==0){
                RNToasty.Warn({title:Strings.pleaseSelectFiles,duration:1})
              }else{
                //this.uploadReport()
                this.createReport()
              }
            }}
            style={{alignSelf:'center',marginTop:moderateScale(10), justifyContent:'center', borderRadius:moderateScale(2), width:wp(80),height:responsiveHeight(7),backgroundColor:'#946AB3', alignItems:'center'}}>
                 <Text style={{ marginHorizontal:moderateScale(2), fontSize:responsiveFontSize(7), color:'white', fontFamily:englishFont}}>{Strings.upload} </Text>
            </TouchableOpacity>
        )
    }

    finishButton=()=>{
        const {isRTL} = this.props
        const {media,selectedDate,agreeDate} = this.state
        return (
            <TouchableOpacity 
            onPress={()=>{resetTo('AdminstraionHome')}}
            style={{alignSelf:'center',marginTop:moderateScale(5),marginBottom:moderateScale(15), justifyContent:'center', borderRadius:moderateScale(2), width:wp(80),height:responsiveHeight(7),backgroundColor:colors.darkBlue, alignItems:'center'}}>
                 <Text style={{ marginHorizontal:moderateScale(2), fontSize:responsiveFontSize(7), color:'white', fontFamily:englishFont}}>{Strings.finish} </Text>
            </TouchableOpacity>
        )
    }
    
    page2=()=>{
        const {isRTL} = this.props
        return (
            <ScrollView showsVerticalScrollIndicator={false} style={{flex:1,backgroundColor:'transparent',marginTop:moderateScale(6)}}>

                <Text style={{alignSelf:'center',marginTop:moderateScale(10), fontSize:responsiveFontSize(9), color:'black', fontFamily:englishFont}}>{Strings.uploadDocs} </Text>
                {this.mediaTypeBox()}
                {this.mediaItem(this.state.media)}         
                {this.uploadButton()}
                {this.finishButton()}
                {this.mediaItem(this.state.allMedia,true)}

            </ScrollView>
        )
    }


    render() {
        const {isRTL} = this.props
        const {page} = this.state 
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                <AppHomeHeader />
                {page==1&&this.page1()}
                {page==2&&this.page2()}
                {this.state.loading&&<LoadingDialogOverlay title={Strings.wait}/>}
                {this.props.showMenuModal&&<Menu/>}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(UploadReports);

