import React, { Component } from 'react';
import {
    View,FlatList,RefreshControl,ActivityIndicator,Easing, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppHomeHeader from '../components/AppHomeHeader'
import Menu from '../components/Menu'
import ApllicationCard from '../components/ApllicationCard'
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Spiner from '../common/Spiner'
import Checkbox from 'react-native-custom-checkbox';


class DetermineApplicationContactTimeWithConsultant extends Component {

    state={
        applicationId:this.props.data?this.props.data.applicationId:null,
        time:null,
        loading:false,
    }

    componentDidMount(){
        this.circularProgress.animate(100, 1000,Easing.linear);
      
        this.intervalId = setInterval(
          () => this.circularProgress.reAnimate(0,100, 1000,Easing.linear),
          1000
        );
      }
      
      componentWillUnmount() {
        clearInterval(this.intervalId);
      }

      timeSection = () => {
          const {isRTL} = this.props
          const {time} = this.state
          return(
            <View style={{width:wp(90),marginTop:moderateScale(15),alignSelf:'center'}}>
                        
                <Text style={{ fontSize:responsiveFontSize(7), color:'#946AB3', fontFamily:boldFont}}>{Strings.selectTime}</Text>

                <View 
                    style={{marginTop:moderateScale(5),width:wp(90),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <Checkbox            
                    checked={time=='From 10 AM to 2 PM'?true:false}
                    style={{marginHorizontal:moderateScale(6), borderColor:'#F79EFE', backgroundColor:'white',color:'#946AB3',borderRadius:moderateScale(2)}}
                    onChange={(name, checked) => {
                        if(checked){
                            this.setState({time:'From 10 AM to 2 PM'})
                        }else{
                            this.setState({time:null})
                        }
                    }}
                    />
                    <Text style={{textAlign:isRTL?'right':'left',width:wp(80),marginHorizontal:moderateScale(1), fontSize:responsiveFontSize(6), color:'#818181', fontFamily:boldFont}}>{Strings.time1} </Text>
                </View>


                <View 
                style={{marginTop:moderateScale(5),width:wp(90),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <Checkbox 
                     onChange={(name, checked) => {
                        if(checked){
                            this.setState({time:'From 2 PM to 6 PM'})
                        }else{
                            this.setState({time:null})
                        }
                    }}           
                    checked={time=='From 2 PM to 6 PM'?true:false}
                    style={{textAlign:isRTL?'right':'left',marginHorizontal:moderateScale(6), borderColor:'#F79EFE', backgroundColor:'white',color:'#946AB3',borderRadius:moderateScale(2)}}
                    />
                    <Text style={{width:wp(80),marginHorizontal:moderateScale(1), fontSize:responsiveFontSize(6), color:'#818181', fontFamily:boldFont}}>{Strings.time2} </Text>
                </View>

                <View 
                style={{marginTop:moderateScale(5),width:wp(90),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <Checkbox   
                     onChange={(name, checked) => {
                        if(checked){
                            this.setState({time:'From 6 PM to 10 PM'})
                        }else{
                            this.setState({time:null})
                        }
                    }}          
                    checked={time=='From 6 PM to 10 PM'?true:false}
                    style={{marginHorizontal:moderateScale(6), borderColor:'#F79EFE', backgroundColor:'white',color:'#946AB3',borderRadius:moderateScale(2)}}
                    />
                    <Text style={{textAlign:isRTL?'right':'left',width:wp(80),marginHorizontal:moderateScale(1), fontSize:responsiveFontSize(6), color:'#818181', fontFamily:boldFont}}>{Strings.time3} </Text>
                </View>

            </View>
          )
      }

      applicationStatusChannge = (id) =>{
        console.log("id3   ",id)
        this.setState({loading:true })
        var data={
            status:'Consultant-CallConfirm',
            'id':id,
            'callingTime':this.state.time
        }
        axios.put(`${BASE_END_POINT}applications`,data,{
            headers: {
                "Content-Type": 'application/json',
                "Authorization": `authorization ${this.props.currentUser.token}`
                
            }
        })
        .then(response=>{
            console.log('change application status   ',response)
            this.setState({loading:false})
            RNToasty.Success({duration:1,title:Strings.sendCallingTime2})
            //push('Rate',{applicationId:this.state.applicationId})
        })
        .catch(error=>{
            this.setState({loading:false})
            console.log('error   ',error.response)
            console.log('error2   ',error)
        })
    }

      sendButton = () => {
        const { isRTL } = this.props
        const {time} = this.state
        return (
            <TouchableOpacity
            onPress={() => {
                if(time){
                    this.applicationStatusChannge(this.state.applicationId)
                }else{
                    RNToasty.Warn({title:Strings.pleaseSelectTime})
                }
             }}
             style={{flexDirection:isRTL?'row-reverse':'row', alignSelf: 'center', marginVertical: moderateScale(15), height: responsiveHeight(6), width: wp(60), justifyContent: 'center', alignItems: 'center', backgroundColor: '#946AB3', borderRadius: moderateScale(8) }} >
                <Text style={{fontSize:responsiveFontSize(6), color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.confirm}</Text>
            </TouchableOpacity>
        )
    }


    render() {
        const {isRTL} = this.props
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                <AppHomeHeader home />
                <ScrollView showsVerticalScrollIndicator={false} style={{flex:1}}>
                    
                    <View style={{alignSelf:'center', marginTop:moderateScale(10)}}>
                    <AnimatedCircularProgress
                    size={100}
                    width={13}
                    fill={0}
                    //duration={1000}
                    ref={(ref) => this.circularProgress = ref}
                    arcSweepAngle={360}
                    rotation={360}
                    lineCap="round"
                    onAnimationComplete={()=>{
                       
                    }}
                    tintColor='#946AB3'
                    backgroundColor='transparent'
                    />
                    </View>
                   
                    <Text style={{alignSelf:'center',width:wp(60),textAlign:'center', marginTop:moderateScale(5),fontSize:responsiveFontSize(9), color:'#707070', fontFamily:boldFont}}>{Strings.msg2}</Text>

                    <View style={{alignSelf:'center',marginTop:moderateScale(10)}}>
                        <Icon name='phone' type='FontAwesome' style={{color:'#946AB3',fontSize:responsiveFontSize(20)}} />
                    </View>

                    {!this.props.data.hideButtons&&this.timeSection()}

                    {!this.props.data.hideButtons&&this.sendButton()}
                  

                </ScrollView>
                {this.state.loading&&<LoadingDialogOverlay title={Strings.wait}/>}
                {this.props.showMenuModal&&<Menu/>}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(DetermineApplicationContactTimeWithConsultant);

