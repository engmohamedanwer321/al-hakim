import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView,Modal, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppCommanHeader from '../components/AppCommanHeader'
import Menu from '../components/Menu'
import { WebView } from 'react-native-webview';
import ImageViewer from 'react-native-image-zoom-viewer';

class MdtTeam extends Component {

    state={
        img:require('../assets/imgs/mdt1.jpg'),
        showModal:false,
    }
    componentDidMount() {
        enableSideMenu(false, null)
    }

    registerButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => { /*this.login()*/ }} style={{flexDirection:isRTL?'row-reverse':'row', alignSelf: 'center', marginTop: moderateScale(15), height: responsiveHeight(7), width: wp(80), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.white, borderRadius: moderateScale(5) }} >
                <Icon name='square-edit-outline' type='MaterialCommunityIcons' style={{color:colors.lightBlue,fontSize:responsiveFontSize(8)}} />
                <Text style={{fontSize:responsiveFontSize(8),marginHorizontal:moderateScale(2),  color: colors.lightBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.save}</Text>
            </TouchableOpacity>
        )
    }

    imageModal = () =>{
        const { isRTL } = this.props
        return (
            <Modal 
            visible={this.state.showModal}
            animated
            onRequestClose={()=>this.setState({showModal:false,})}
            >
            <View style={{flex:1,backgroundColor:'#946AB3'}}>
               <TouchableOpacity 
               onPress={()=>{
                   this.setState({showModal:false})
               }}
               style={{alignSelf:'flex-end', margin:moderateScale(8), justifyContent:'center',alignItems:'center'}} >
                   <Icon name='close' type='AntDesign' style={{color:'white'}} />
               </TouchableOpacity>

                <ImageViewer 
                backgroundColor='#946AB3'
                imageUrls={[{
                    width:wp(100),
                    height:responsiveHeight(25),
                     props: {
                        source:this.state.img
                    }
                }]}
                />
               

           </View>
           </Modal>
        )
    }

    render() {
        const {isRTL} = this.props
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                
                <AppCommanHeader title={Strings.mdtTeam} />
          
                <ScrollView showsVerticalScrollIndicator={false}>

                    <View style={{padding:moderateScale(10),paddingBottom:moderateScale(10), backgroundColor:"#946AB3",width:wp(90),borderRadius:moderateScale(9),alignSelf:'center',marginTop:moderateScale(5),marginBottom:moderateScale(20)}} >
                        <FastImage
                            source={require('../assets/imgs/mdtTeam.png')}
                            style={{width:wp(80),height:responsiveHeight(25),borderRadius:moderateScale(6)}}
                        />

                        <Text style={{marginTop:moderateScale(20),fontSize:responsiveFontSize(9), color:'white', fontFamily:isRTL?arrabicFont:englishFont }}>
                        Prof. Sherif Omar
                        {'\n'}
                        <Text style={{fontSize:responsiveFontSize(7)}}>
                        Professor Of Surgical Oncology{'\n'}
                        Former Dean National Cancer Institute 
                        </Text>
                        {'\n'}{'\n'}
                        Prof. Hussein Khaled
                        {'\n'}
                        <Text style={{fontSize:responsiveFontSize(7)}}>
                        Professor of Medical Oncology{'\n'}
                        Former Dean National Cancer Institute
                        </Text>
                        {'\n'}{'\n'}
                        Prof. Mohamed Lotayef
                        {'\n'}
                        <Text style={{fontSize:responsiveFontSize(7)}}>
                        Professor of Radiation Oncology{'\n'}
                        Former Dean National Cancer Institute
                        </Text>
                        {'\n'}{'\n'}
                        Prof. Omar Sherif Omar{'\n'}
                        <Text style={{fontSize:responsiveFontSize(7)}}>
                        Associate Professor Of Surgery,{'\n'}
                        Cairo university{'\n'}
                        Consultant Oncoplastic Breast Surgeon{'\n'}
                        Medical Director – Arab Breast & Cancer Care
                        </Text>
                        {'\n'}{'\n'}
                        Prof. Wessam El Sherif{'\n'}
                        <Text style={{fontSize:responsiveFontSize(7)}}>
                        Associate Professor Of Clinical Oncology{'\n'}
                        Cairo university{'\n'}
                        Medical Director – ABC Day Care
                        </Text>
                        
                        </Text>

                        <WebView 
                        javaScriptEnabled
                        allowsFullscreenVideo
                        style={{width:wp(80),marginTop:moderateScale(20), height:responsiveHeight(38)}}
                        source={{ uri: 'https://www.youtube.com/embed/1F1ba8mE6Mc' }} />
                    
                        
                        <TouchableOpacity onPress={()=>{
                            this.setState({showModal:true,img:require('../assets/imgs/mdt1.jpg')})
                        }}>
                            <FastImage
                                source={require('../assets/imgs/mdt1.jpg')}
                                style={{marginTop:moderateScale(10), width:wp(80),height:responsiveHeight(25),borderRadius:moderateScale(6)}}
                            />
                        </TouchableOpacity>
                       
                        <TouchableOpacity onPress={()=>{
                            this.setState({showModal:true, img:require('../assets/imgs/mdt2.jpg')})
                        }}>
                            <FastImage
                                source={require('../assets/imgs/mdt2.jpg')}
                                style={{marginTop:moderateScale(10), width:wp(80),height:responsiveHeight(25),borderRadius:moderateScale(6)}}
                            />
                        </TouchableOpacity>

                    </View>

                </ScrollView>

                {this.imageModal()}
               {/*this.props.showMenuModal&&<Menu/>*/}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(MdtTeam);

