import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import Dialog, {DialogTitle, DialogContent,SlideAnimation } from 'react-native-popup-dialog';
import {hasValue,isEmail,isPhone,isLength,isNumber} from '../validations/validations'

class Login extends Component {

    state = {
        email: ' ',
        password: ' ',
        hidePassword: true,
        loading: false,
        showDialog:false,
    }

    componentDidMount() {
        enableSideMenu(false, null)
    }

    dialog=()=>{
        const { isRTL } = this.props
        return (
            <Dialog
            visible={this.state.showDialog}
            dialogAnimation={new SlideAnimation({
              slideFrom: 'bottom',
            })}
            onTouchOutside={()=>this.setState({showDialog:false})}
            dialogTitle={<DialogTitle title={Strings.elkhabir}/>}
            width={wp(80)}
            height={responsiveHeight(55)}
          >
            <View style={{width:wp(80),height:responsiveHeight(55)}}>
                <FastImage
                resizeMode='contain'
                source={require('../assets/imgs/appLogo.png')}
                style={{width:wp(80),height:responsiveHeight(20)}}
                />

                <Text style={{marginVertical:moderateScale(5), alignSelf:'center',width:wp(60),textAlign:'center', fontSize:responsiveFontSize(8),marginHorizontal:moderateScale(2),  color: 'black', fontFamily: boldFont }}>{Strings.mustBeVerified}</Text>
                
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({showDialog:false})
                    //push('MakeApplication')
                }} 
                style={{borderRadius:moderateScale(5), width:wp(40),marginTop:moderateScale(6), alignSelf:'center',height:responsiveHeight(8),backgroundColor:'#946AB3',justifyContent:'center',alignItems:'center'}}>
                    <Text style={{fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(2),  color: colors.white, fontFamily: boldFont }}>{Strings.ok}</Text>
                </TouchableOpacity>

            </View>
          </Dialog>
        ) 
    }

    Image_Title = () => {
        const { isRTL } = this.props
        return (
            <View style={{ marginTop: moderateScale(20), alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
               <FastImage
               resizeMode='contain'
                source={require('../assets/imgs/appLogo.png')}
                style={{width:wp(80),height:responsiveHeight(30)}}
                />
            </View>
        )
    }

    emailInput = () => {
        const { isRTL } = this.props
        const { email } = this.state
        return (
            <View style={{marginTop: moderateScale(15), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(80)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.email}</Label>    
                    <Input
                        keyboardType='email-address'
                        onChangeText={(val) => { this.setState({ email: val }) }}
                        style={{fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.email}
                    />
                </Item>

                {email.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    passwordInput = () => {
        const { isRTL } = this.props
        const { password, hidePassword } = this.state
        return (
            <View style={{ marginTop: moderateScale(6), width: wp(80), alignSelf: 'center', justifyContent: 'center', alignItems: 'center' }}>
               <Item floatingLabel style={{ borderBottomColor:colors.darkGray,borderBottomWidth:0.5, flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: responsiveWidth(80)}}>

                    <Label style={{color:colors.darkGray}} >{Strings.password}</Label>    
                    <Input
                        secureTextEntry
                        onChangeText={(val) => { this.setState({ password: val }) }}
                        style={{fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(80), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.password}
                    />
                </Item>
               
               
                {password.length == 0 &&
                    <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    getUserByToken = (token) => {
        axios.get(`${BASE_END_POINT}users/get_user`, {
            headers: {
                "Authorization": `authorization ${token}`
            }
        })
        .then(response => {
            this.setState({ loading: false })
            console.log("getUserByToken   ",response.data)
            if(response.data.verified==0){
                this.setState({showDialog:true})
            }else{
                const user ={...response.data,token:token}
                this.props.setUser(user)
                AsyncStorage.setItem('USER', JSON.stringify(user))
                resetTo('Home')
                /*if(user.role=='administration'){ //administration
                    resetTo('AdminstraionHome')
                }else{
                    resetTo('Home')
                }*/
            }
         
        })
        .catch(error=>{
            this.setState({ loading: false })
            console.log("error  ",error)
        })
    }

    login = () => {
        const { password, email } = this.state
        if (!email.replace(/\s/g, '').length) {
            this.setState({ email: '' })
        }
        if (!password.replace(/\s/g, '').length) {
            this.setState({ password: '' })
        }
        if (email.replace(/\s/g, '').length && password.replace(/\s/g, '').length) {
            this.setState({ loading: true })
            var data= new URLSearchParams()
            data.append('email',email)
            data.append('password',password)
            axios.post(`${BASE_END_POINT}users/authenticate`,data, {
                headers: {
                    "Content-Type": 'application/x-www-form-urlencoded'
                }
            })
            .then(response => {
                this.setState({ loading: false })
                console.log("login   ",response.data)
                this.getUserByToken(response.data.data.token)     
            })
            .catch(error => {
                this.setState({ loading: false })
                console.log("ERROR   ",error.response)
                if (error.response.status == 400) {
                    RNToasty.Error({ title: strings.loginUserDataIncorrect,duration:1})
                }
            })
        }

    }

    loginButton = () => {
        const { isRTL } = this.props

        return (
            <Button onPress={() => { 
                this.login()
            }} style={{ alignSelf: 'center', marginTop: moderateScale(15), height: responsiveHeight(7), width: wp(80), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.darkBlue, borderRadius: moderateScale(5) }} >
                <Text style={{fontSize:responsiveFontSize(7),  color: colors.white, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.signin}</Text>
            </Button>
        )
    }

    forgetPasswordSection = () => {
        const { isRTL } = this.props
        return (
            <View style={{width:wp(80),marginTop:moderateScale(6),alignSelf:'center'}}>
                <TouchableOpacity onPress={()=>{
                    push('ForgetPassword')
                }}>
                    <Text style={{alignSelf:isRTL?'flex-end':'flex-start', fontSize:responsiveFontSize(8), color: colors.darkGray, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.forgetPassword}</Text>
                </TouchableOpacity>
            </View>
        )
    }

    registerButton = () => {
        const { isRTL } = this.props

        return (
            <View style={{marginTop:moderateScale(35), alignSelf:'center'}}>
                <TouchableOpacity onPress={()=>{
                    push('Register')
                }}>
                    <Text style={{fontSize:responsiveFontSize(10), color: colors.darkBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.register2}</Text>
                </TouchableOpacity>
            </View>
        )
    }




    render() {
        const { loading } = this.state
        return (
            <View showsVerticalScrollIndicator={false} style={{backgroundColor:colors.grayBackground,flex:1}}>

                <ScrollView style={{marginBottom:moderateScale(5)}} showsVerticalScrollIndicator>
                    {this.Image_Title()}
                    {this.emailInput()}
                    {this.passwordInput()}
                    {this.loginButton()}
                    {this.forgetPasswordSection()}
                    {this.registerButton()}
                    {this.dialog()}
                </ScrollView>  

                {loading &&
                    <LoadingDialogOverlay title={Strings.wait} />
                }

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,

    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(Login);

