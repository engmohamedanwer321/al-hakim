import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  ScrollView,
  TextInput,
  Alert,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import {Icon, Button, Item, Input, Label} from 'native-base';
import {setUser} from '../actions/AuthActions';
import {
  responsiveHeight,
  responsiveWidth,
  moderateScale,
  responsiveFontSize,
} from '../utils/responsiveDimensions';
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import {
  enableSideMenu,
  resetTo,
  push,
  pop,
} from '../controlls/NavigationControll';
import {arrabicFont, englishFont, boldFont} from '../common/AppFont';
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import {removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image';
import {RNToasty} from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {BASE_END_POINT} from '../AppConfig';
import strings from '../assets/strings';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import AppHomeHeader from '../components/AppHomeHeader';
import Menu from '../components/Menu';
import Dialog, {
  DialogTitle,
  DialogContent,
  SlideAnimation,
} from 'react-native-popup-dialog';

class Home extends Component {
  state = {
    showDialog: false,
    openNewApllication:this.props.currentUser.role=='patient'?false:true,
  };

  componentDidMount() {
    enableSideMenu(false, null);
    if(this.props.currentUser.role=='patient'){
      this.getApplications()
    }
  }

  getApplications = () => {
    axios.get(`${BASE_END_POINT}applications/users/${this.props.currentUser.id}`,{
        headers:{
            "Authorization": `authorization ${this.props.currentUser.token}`
        }
    })
    .then(response=>{
      console.log('apps   ',response.data.data)
      var apps = response.data.data.filter(application=>application.status!="Completed")
      if(apps.length>0){
        console.log('Close Apps')
        this.setState({openNewApllication:true})
      }else{
        console.log('Open Apps')
        this.setState({openNewApllication:true})
      }
    })
    .catch(error=>{
      console.log('Error   ',error.response)
      this.setState({openNewApllication:false,})
    })
  }

  dialog = () => {
    const {isRTL} = this.props;
    return (
      <Dialog
        visible={this.state.showDialog}
        dialogAnimation={
          new SlideAnimation({
            slideFrom: 'bottom',
          })
        }
        onTouchOutside={() => this.setState({showDialog: false})}
        dialogTitle={<DialogTitle title={Strings.elkhabir} />}
        width={wp(80)}
        height={responsiveHeight(55)}>
        <View style={{width: wp(80), height: responsiveHeight(55)}}>
          <FastImage
            resizeMode="contain"
            source={require('../assets/imgs/appLogo.png')}
            style={{width: wp(80), height: responsiveHeight(20)}}
          />

          <TouchableOpacity
            onPress={() => {
              this.setState({showDialog: false});
              push('MakeApplication');
            }}
            style={{
              borderRadius: moderateScale(5),
              width: wp(70),
              marginTop: moderateScale(6),
              alignSelf: 'center',
              height: responsiveHeight(8),
              backgroundColor: '#946AB3',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: responsiveFontSize(5.5),
                marginHorizontal: moderateScale(2),
                color: colors.white,
                fontFamily: boldFont,
              }}>
              {Strings.createNewApp}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => {
              this.setState({showDialog: false});
              push('MyApplications');
            }}
            style={{
              borderRadius: moderateScale(5),
              width: wp(70),
              marginTop: moderateScale(7),
              alignSelf: 'center',
              height: responsiveHeight(8),
              backgroundColor: colors.darkBlue,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                fontSize: responsiveFontSize(5.5),
                marginHorizontal: moderateScale(2),
                color: colors.white,
                fontFamily: boldFont,
              }}>
              {Strings.previousApllications}
            </Text>
          </TouchableOpacity>
        </View>
      </Dialog>
    );
  };

  registerButton = () => {
    const {isRTL} = this.props;

    return (
      <TouchableOpacity
        onPress={() => {
          /*this.login()*/
        }}
        style={{
          flexDirection: isRTL ? 'row-reverse' : 'row',
          alignSelf: 'center',
          marginTop: moderateScale(15),
          height: responsiveHeight(7),
          width: wp(80),
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: colors.white,
          borderRadius: moderateScale(5),
        }}>
        <Icon
          name="square-edit-outline"
          type="MaterialCommunityIcons"
          style={{color: colors.lightBlue, fontSize: responsiveFontSize(8)}}
        />
        <Text
          style={{
            fontSize: responsiveFontSize(8),
            marginHorizontal: moderateScale(2),
            color: colors.lightBlue,
            fontFamily: isRTL ? arrabicFont : englishFont,
          }}>
          {Strings.save}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    const {isRTL,currentUser} = this.props;
    return (
      <View style={{backgroundColor: colors.lightGray, flex: 1}}>
        <AppHomeHeader hideBack />

        <View
          style={{
            alignSelf: 'flex-end',marginTop: moderateScale(5),marginHorizontal: moderateScale(6),flexDirection: isRTL ? 'row-reverse' : 'row',alignItems: 'center', justifyContent: 'center',}}
          >
          <Text
            style={{
              fontSize: responsiveFontSize(6),
              marginHorizontal: moderateScale(2),
              color: colors.darkBlue,
              fontFamily: boldFont,
            }}>
            Al-Hakim
          </Text>
          <FastImage
            source={require('../assets/imgs/profileicon.jpg')}
            style={{
              borderWidth: 3,
              borderColor: '#946AB3',
              width: 30,
              height: 30,
              borderRadius: 15,
            }}
          />
        </View>

        <Animatable.View animation="slideInRight" delay={400}>
          <TouchableOpacity
            style={{
              width: wp(85),
              marginTop: moderateScale(10),
              alignSelf: 'flex-end',
              borderTopLeftRadius: moderateScale(10),
              borderBottomLeftRadius: moderateScale(10),
              height: responsiveHeight(8),
              backgroundColor: '#946AB3',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                textAlign: isRTL ? 'right' : 'left',
                fontSize: responsiveFontSize(7),
                marginHorizontal: moderateScale(2),
                color: colors.white,
                fontFamily: englishFont,
              }}>
              {Strings.help}
            </Text>
          </TouchableOpacity>
        </Animatable.View>

        <TouchableOpacity
          onPress={() => {
            //this.setState({showDialog: true});
            
             if(this.props.currentUser.role!='patient'){
                  push('MakeApplication');
             }else{
                if(this.state.openNewApllication){
                  //this.setState({showDialog: true});
                  push('MakeApplication');
                }else{
                  RNToasty.Info({title:Strings.youCantCreateNewApp,duration:1})
                }
             }
             
            
          }}
          style={{
            borderRadius: moderateScale(10),
            width: wp(70),
            marginTop: moderateScale(10),
            alignSelf: 'center',
            height: responsiveHeight(8),
            backgroundColor: colors.darkBlue,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: responsiveFontSize(6),
              marginHorizontal: moderateScale(2),
              color: colors.white,
              fontFamily: boldFont,
            }}>
            {currentUser&&currentUser.role!='patient'?Strings.NewPatient:Strings.getStarted}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => {
            push('MyApplications');
          }}
          style={{
            borderRadius: moderateScale(10),
            width: wp(70),
            marginTop: moderateScale(10),
            alignSelf: 'center',
            height: responsiveHeight(8),
            backgroundColor: colors.darkBlue,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontSize: responsiveFontSize(6),
              marginHorizontal: moderateScale(2),
              color: colors.white,
              fontFamily: boldFont,
            }}>
            {currentUser&&currentUser.role!='patient'?Strings.previousPatients:Strings.previousRecore}
          </Text>
        </TouchableOpacity>

        <TouchableOpacity
          disabled
          style={{
            borderRadius: moderateScale(10),
            width: wp(70),
            marginTop: moderateScale(10),
            alignSelf: 'center',
            height: responsiveHeight(8),
            backgroundColor: colors.darkBlue,
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection:isRTL?'row-reverse':'row'
          }}>
          <Text style={{fontSize: responsiveFontSize(6),color: colors.white,fontFamily: boldFont,}}>
            {Strings.educational} 
          </Text>

          <Text style={{fontSize:12,marginTop:moderateScale(-10),color:'white',fontFamily: boldFont, marginHorizontal:moderateScale(2)}} >{Strings.soon}</Text>
        </TouchableOpacity>

        {this.dialog()}

        {this.props.showMenuModal && <Menu />}
      </View>
    );
  }
}
const mapDispatchToProps = {
  setUser,
  removeItem,
};

const mapToStateProps = state => ({
  isRTL: state.lang.RTL,
  showMenuModal: state.menu.showMenuModal,
  currentUser: state.auth.currentUser,
  //userToken: state.auth.userToken,
});

export default connect(
  mapToStateProps,
  mapDispatchToProps,
)(Home);
