import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppHomeHeader from '../components/AppHomeHeader'
import Menu from '../components/Menu'
import PDFView from 'react-native-view-pdf'
import RNFetchBlob from 'rn-fetch-blob'
import RNFS  from 'react-native-fs'
import {check,request, PERMISSIONS, RESULTS} from 'react-native-permissions'
import moment from 'moment'


class ShowAllReports extends Component {

    state = {
        loading: false,
    }

    componentDidMount() {
        enableSideMenu(false, null)
        this.ckeckPermission()
    }

    ckeckPermission = () => {
        check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
        .then((result) => {
            switch (result) {
            case RESULTS.UNAVAILABLE:
                console.log('This feature is not available (on this device / in this context)',);
                request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
                break;
            case RESULTS.DENIED:
                console.log('The permission has not been requested / is denied but requestable',);
                request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
                break;
            case RESULTS.GRANTED:
                console.log('The permission is granted');
                break;
            case RESULTS.BLOCKED:
                console.log('The permission is denied and not requestable anymore');
                request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
                break;
            }
        })
        .catch((error) => {
            // …
        });
    }

    extention = (filename) => {
        return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
      }

    download = (uri) => {
        this.setState({loading:true})
        let dirs = RNFetchBlob.fs.dirs
        var ext = this.extention(uri);
        ext = "."+ext[0];
        RNFS.downloadFile({
            fromUrl: uri,        
            toFile: dirs.DownloadDir + "/file_"+(new Date().getTime())+ext,
        })
        .promise.then(res=>{
            console.log("DONE   ",res)
            RNToasty.Success({title:'Download done...'})
            this.setState({loading:false})
        }) 
      }


    downLoadButtons = () =>{
        const { isRTL,data } = this.props
        return(
            <View style={{flex:1,justifyContent:'center',alignItems:'center', alignSelf:'center',}}>
               
               <Text style={{marginBottom:moderateScale(15), fontSize:responsiveFontSize(10),  color:"black", fontFamily:boldFont }}>{this.props.data.name}</Text>
               
                <TouchableOpacity onPress={()=>{
                    this.download(this.props.data.files[this.props.data.files.length-2].filePath)
                }}
                style={{flexDirection:isRTL?'row-reverse':'row', width:wp(80),height:responsiveHeight(8),justifyContent:'center',alignItems:'center',borderRadius:moderateScale(10),backgroundColor:colors.darkBlue}} >
                    <Icon name='file-pdf' type='MaterialCommunityIcons' style={{color:'white'}}  />
                    <Text style={{marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(7),  color:"white", fontFamily:boldFont }}>{Strings.downloadIntialReport}</Text>
                </TouchableOpacity>


                <TouchableOpacity onPress={()=>{
                    this.download(this.props.data.files[this.props.data.files.length-1].filePath)
                }}
                style={{marginTop:moderateScale(10), flexDirection:isRTL?'row-reverse':'row', width:wp(80),height:responsiveHeight(8),justifyContent:'center',alignItems:'center',borderRadius:moderateScale(10),backgroundColor:'#946AB3'}} >
                    <Icon name='file-pdf' type='MaterialCommunityIcons' style={{color:'white'}}  />
                    <Text style={{marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(7),  color:"white", fontFamily:boldFont }}>{Strings.downloadFinalReport}</Text>
                </TouchableOpacity>

                
            </View>
        )
    }



    render() {
        const { loading } = this.state
        const { isRTL,currentUser,data } = this.props
        moment.locale(isRTL?'are':'en')
        return (
            <View style={{backgroundColor:colors.grayBackground,flex:1}}>
                <AppHomeHeader title={Strings.contactUs} />
                 {this.downLoadButtons()}   
                {this.props.showMenuModal&&<Menu/>}

                {loading &&
                    <LoadingDialogOverlay title={Strings.wait} />
                }

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser: state.auth.currentUser,
})


export default connect(mapToStateProps, mapDispatchToProps)(ShowAllReports);

