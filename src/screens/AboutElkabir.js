import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppCommanHeader from '../components/AppCommanHeader'
import Menu from '../components/Menu'

class AboutElkabir extends Component {

  

    componentDidMount() {
        enableSideMenu(false, null)
    }

   

    
    registerButton = () => {
        const { isRTL } = this.props

        return (
            <TouchableOpacity onPress={() => { /*this.login()*/ }} style={{flexDirection:isRTL?'row-reverse':'row', alignSelf: 'center', marginTop: moderateScale(15), height: responsiveHeight(7), width: wp(80), justifyContent: 'center', alignItems: 'center', backgroundColor: colors.white, borderRadius: moderateScale(5) }} >
                <Icon name='square-edit-outline' type='MaterialCommunityIcons' style={{color:colors.lightBlue,fontSize:responsiveFontSize(8)}} />
                <Text style={{fontSize:responsiveFontSize(8),marginHorizontal:moderateScale(2),  color: colors.lightBlue, fontFamily: isRTL ? arrabicFont : englishFont }}>{Strings.save}</Text>
            </TouchableOpacity>
        )
    }


  


    render() {
        const {isRTL} = this.props
        const about =  
        isRTL?
        ` برنامج الحكيم هو تطبيق هدفه الأول هو إدارة المنظومة الطبية لوقاية و المنشآت التابعة لها عن طريق جمع التقارير الطبية و دراستها و أخذ قرارات بنيت على دراسة ووعي كامل بإحتياجاتنا للوصول إلى أفضل أداء لكافة الكوادر الطبية و من ثم الإرتقاء بالخدمه الطبية المقدمه .`
        :
        `Al-Hakim program is an application whose aim is to manage the medical system for Weqaya and its affiliations by collecting and studying medical reports and taking decisions based on a study and full awareness of our needs to reach the best performance for all medical personnel and then improve the medical service provided.`
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                
                <AppCommanHeader title={Strings.aboutElkhabir} />
          
                <ScrollView showsVerticalScrollIndicator={false}>
                    <View style={{alignSelf:isRTL?"flex-end":'flex-start',marginTop:moderateScale(5), marginHorizontal:moderateScale(6),flexDirection:isRTL?'row-reverse':'row',alignItems:'center',justifyContent:'center'}}>
                            <FastImage
                                source={require('../assets/imgs/profileicon.jpg')}
                                style={{borderWidth:3,borderColor:'#946AB3',width:30,height:30,borderRadius:15}}
                            />
                            <Text style={{fontSize:responsiveFontSize(6),marginHorizontal:moderateScale(2),  color: colors.darkBlue, fontFamily: boldFont }}>Al-Hakim</Text>
                        </View>

                    <View style={{padding:moderateScale(10),paddingBottom:moderateScale(40), backgroundColor:"#946AB3",width:wp(90),borderRadius:moderateScale(9),alignSelf:'center',marginTop:moderateScale(5),marginBottom:moderateScale(20)}} >
                        <Text style={{marginTop:moderateScale(10), fontSize:responsiveFontSize(9), color:'white', fontFamily:isRTL?arrabicFont:englishFont }}>
                        {about}
                        </Text>
                    </View>

                </ScrollView>



                {/*this.props.showMenuModal&&<Menu/>*/}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(AboutElkabir);

