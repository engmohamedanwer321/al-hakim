import React, {Component} from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  Text,
  ScrollView,
  Linking,
  TextInput,
  Alert,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import {Icon, Button, Item, Input, Label} from 'native-base';
import {setUser} from '../actions/AuthActions';
import {
  responsiveHeight,
  responsiveWidth,
  moderateScale,
  responsiveFontSize,
} from '../utils/responsiveDimensions';
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import {
  enableSideMenu,
  resetTo,
  push,
  pop,
} from '../controlls/NavigationControll';
import {arrabicFont, englishFont, boldFont} from '../common/AppFont';
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import {removeItem} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image';
import {RNToasty} from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
import {BASE_END_POINT} from '../AppConfig';
import strings from '../assets/strings';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import AppCommanHeader from '../components/AppCommanHeader';
import Menu from '../components/Menu';

class PsychologyClinic extends Component {
  componentDidMount() {
    enableSideMenu(false, null);
  }

  registerButton = () => {
    const {isRTL} = this.props;

    return (
      <TouchableOpacity
        onPress={() => {
          /*this.login()*/
        }}
        style={{
          flexDirection: isRTL ? 'row-reverse' : 'row',
          alignSelf: 'center',
          marginTop: moderateScale(15),
          height: responsiveHeight(7),
          width: wp(80),
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: colors.white,
          borderRadius: moderateScale(5),
        }}>
        <Icon
          name="square-edit-outline"
          type="MaterialCommunityIcons"
          style={{color: colors.lightBlue, fontSize: responsiveFontSize(8)}}
        />
        <Text
          style={{
            fontSize: responsiveFontSize(8),
            marginHorizontal: moderateScale(2),
            color: colors.lightBlue,
            fontFamily: isRTL ? arrabicFont : englishFont,
          }}>
          {Strings.save}
        </Text>
      </TouchableOpacity>
    );
  };

  render() {
    const {isRTL} = this.props;
    return (
      <View style={{backgroundColor: colors.lightGray, flex: 1}}>
        <AppCommanHeader title={Strings.psychologyClinc} />

        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              paddingBottom: moderateScale(20),
              backgroundColor: '#946AB3',
              width: wp(90),
              borderRadius: moderateScale(6),
              alignSelf: 'center',
              marginTop: moderateScale(5),
              marginBottom: moderateScale(20),
            }}>
            <View
              style={{
                alignItems:'center',
                alignSelf: 'center',
                width: wp(80),
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: moderateScale(10),
              }}>
              <View>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(9),
                    marginTop: moderateScale(5),
                    color: colors.white,
                    fontFamily: englishFont,
                  }}>
                  Dr
                </Text>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(8),
                    marginTop: moderateScale(0),
                    color: colors.white,
                    fontFamily: englishFont,
                  }}>
                  Stephanie Ghanem
                </Text>

                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(7),
                    marginTop: moderateScale(5),
                    color: colors.white,
                    fontFamily: englishFont,
                  }}>
                  Online Psychology Consultant Cancer Psychology Clinic - Beirut
                </Text>
              </View>

              <TouchableOpacity>
                <FastImage
                  //resizeMode='contain'
                  source={require('../assets/imgs/drStaphanie.jpg')}
                  style={{
                    flex: 1,
                    minHeight:responsiveHeight(33),
                    width: wp(38),
                    borderRadius: moderateScale(5),
                  }}
                />
              </TouchableOpacity>
            </View>

            <View
              style={{
                alignSelf: 'center',
                alignItems:'center',
                width: wp(80),
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginTop: moderateScale(20),
              }}>
              <View>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(9),
                    marginTop: moderateScale(5),
                    color: colors.white,
                    fontFamily: englishFont,
                  }}>
                  Dr
                </Text>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(8),
                    marginTop: moderateScale(0),
                    color: colors.white,
                    fontFamily: englishFont,
                  }}>
                  Soha Sherif
                </Text>
                <Text
                  style={{
                    maxWidth: wp(40),
                    alignSelf: 'flex-start',
                    fontSize: responsiveFontSize(7),
                    marginTop: moderateScale(5),
                    color: colors.white,
                    fontFamily: englishFont,
                  }}>
                  ABC Psychology Consultant
                </Text>
              </View>

              <TouchableOpacity>
                <FastImage
                  resizeMode="contain"
                  source={require('../assets/imgs/drSohaSami.png')}
                  style={{
                    flex: 1,
                    width: wp(38),
                    height: responsiveHeight(33),
                    borderRadius: moderateScale(5),
                  }}
                />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

        {/*this.props.showMenuModal&&<Menu/>*/}
      </View>
    );
  }
}
const mapDispatchToProps = {
  setUser,
  removeItem,
};

const mapToStateProps = state => ({
  isRTL: state.lang.RTL,
  showMenuModal: state.menu.showMenuModal,
  currentUser: state.auth.currentUser,

  //userToken: state.auth.userToken,
});

export default connect(
  mapToStateProps,
  mapDispatchToProps,
)(PsychologyClinic);
