import React, { Component } from 'react';
import {
    View, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppHomeHeader from '../components/AppHomeHeader'
import Menu from '../components/Menu'
import PDFView from 'react-native-view-pdf';
import RNFetchBlob from 'rn-fetch-blob';
import RNFS  from 'react-native-fs'
import {check,request, PERMISSIONS, RESULTS} from 'react-native-permissions';
import moment from 'moment'

class ShowIntialRoport extends Component {
    
    
    
    state = {
        loading: false,
        resources: {
            file: Platform.OS === 'ios' ? 'downloadedDocument.pdf' : '/sdcard/Download/downloadedDocument.pdf',
            url:this.props.data.files[this.props.data.files.length-1].filePath,
            base64: 'JVBERi0xLjMKJcfs...',
        },
    }

    componentDidMount() {
        enableSideMenu(false, null)
        this.ckeckPermission()
    }

    
    ckeckPermission = () => {
        check(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
        .then((result) => {
            switch (result) {
            case RESULTS.UNAVAILABLE:
                console.log('This feature is not available (on this device / in this context)',);
                request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
                break;
            case RESULTS.DENIED:
                console.log('The permission has not been requested / is denied but requestable',);
                request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
                break;
            case RESULTS.GRANTED:
                console.log('The permission is granted');
                break;
            case RESULTS.BLOCKED:
                console.log('The permission is denied and not requestable anymore');
                request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE)
                break;
            }
        })
        .catch((error) => {
            // …
        });
    }

    extention = (filename) => {
        return (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : undefined;
      }

    download = (uri) => {
        this.setState({loading:true})
        let dirs = RNFetchBlob.fs.dirs
        var ext = this.extention(uri);
        ext = "."+ext[0];
        RNFS.downloadFile({
            fromUrl: uri,        
            toFile: dirs.DownloadDir + "/file_"+(new Date().getTime())+ext,
        })
        .promise.then(res=>{
            console.log("DONE   ",res)
            RNToasty.Success({title:'Download done...'})
            this.setState({loading:false})
        }) 
      }

    donwloadReport = () =>{
        const { isRTL } = this.props
        return(
            <TouchableOpacity 
            onPress={()=>{
                this.download(this.props.data.files[this.props.data.files.length-1].filePath)
            }}
            style={{flexDirection:isRTL?'row-reverse':'row',justifyContent:'center', alignItems:'center', width:wp(70),alignSelf:'center',marginTop:moderateScale(5),}}>
                <FastImage 
                source={require('../assets/imgs/pdf.png')}
                style={{width:wp(8),height:responsiveHeight(5),}}
                />
                <Text style={{width:wp(60),marginTop:moderateScale(1), alignSelf:isRTL?'flex-end':'flex-start', fontSize:responsiveFontSize(8),  color:"black", fontFamily:boldFont }}>{Strings.downloadReport}</Text>
            </TouchableOpacity>
        )
    }

    applicationStatusChannge = () =>{
        console.log("id3   ",this.props.data.id)
        this.setState({loading:true })
        var data={
            status:'MDT-Review',
            'id':this.props.data.id,
        }
        axios.put(`${BASE_END_POINT}applications`,data,{
            headers: {
                "Content-Type": 'application/json',
                "Authorization": `authorization ${this.props.currentUser.token}`
            }
        })
        .then(response=>{
            console.log('change application status   ',response)
            this.setState({loading:false})
            push('MdtReview',this.props.data)
        })
        .catch(error=>{
            this.setState({loading:false})
            console.log('error   ',error.response)
            console.log('error2   ',error)
        })
    }

 

    nextButton = () => {
        const { isRTL } = this.props
        return(
            <TouchableOpacity 
            onPress={()=>{
                this.applicationStatusChannge()
                //push('DetermineApplicationContactTimeWithConsultant')
            }}
            style={{alignSelf:'flex-end', marginVertical:moderateScale(15), justifyContent:'center', borderTopLeftRadius:moderateScale(20),borderBottomLeftRadius:moderateScale(20), width:wp(30),height:responsiveHeight(11),backgroundColor:'#946AB3', flexDirection:'row',alignItems:'center'}}
            >
                <Text style={{ marginHorizontal:moderateScale(2), fontSize:responsiveFontSize(7), color:'white', fontFamily:englishFont}}>{Strings.next} </Text>
                <Icon name='arrowright' type='AntDesign' style={{color:'white',fontSize:responsiveFontSize(10)}} />
            </TouchableOpacity>
        )
    }



    render() {
        const { loading } = this.state
        const { isRTL,data,currentUser } = this.props
        moment.locale(isRTL?'are':'en')
        return (
            <View style={{backgroundColor:colors.grayBackground,flex:1}}>
                <AppHomeHeader  title={Strings.contactUs} />
                <ScrollView 
                showsVerticalScrollIndicator={false}
                style={{width:wp(100),marginTop:moderateScale(5)}}
                nestedScrollEnabled={true}
                >
                    <View style={{backgroundColor:'white', alignSelf:'center', width:wp(80),minHeight:responsiveHeight(60),borderRadius:moderateScale(20)}}>
                        <View style={{width:wp(70),alignSelf:'center', marginTop:moderateScale(10),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                            <FastImage 
                            source={require('../assets/imgs/profileicon.jpg')}
                            style={{width:60,height:60,borderRadius:30}}
                            />
                            <View style={{marginHorizontal:moderateScale(2)}}>
                                <Text style={{width:wp(50), alignSelf:isRTL?'flex-end':'flex-start',textAlign:isRTL?'right':'left', fontSize:responsiveFontSize(7),  color:"black", fontFamily:boldFont }}>{currentUser?currentUser.first_name+" "+currentUser.last_name:''}</Text>
                                <Text style={{width:wp(50),alignSelf:isRTL?'flex-end':'flex-start',textAlign:isRTL?'right':'left', fontSize:responsiveFontSize(6),  color:colors.darkGray, fontFamily:isRTL?arrabicFont:englishFont }}>{moment(data.createdAt).format("MMM Do YYYY")}</Text>
                            </View>
                        </View>
                        <View style={{marginBottom:moderateScale(15), width:wp(70),alignSelf:'center',}}>
                            <Text style={{width:wp(70), alignSelf:isRTL?'flex-end':'flex-start', fontSize:responsiveFontSize(9),  color:"black", fontFamily:boldFont }}>{strings.initialReportPreview}</Text>
                            
                            <View style={{width:wp(70), height:responsiveHeight(60),marginVertical:moderateScale(10),}}>
                                <PDFView
                                fadeInDuration={250.0}
                                style={{flex:1,height:responsiveHeight(60),width:wp(70),}}
                                resource={this.state.resources['url']}
                                resourceType='url'
                                
                                />
                            </View>
                        </View>
                    </View>
                    {this.donwloadReport()}
                    {this.nextButton()}
                    
                </ScrollView>
               
                {this.props.showMenuModal&&<Menu/>}

                {loading &&
                    <LoadingDialogOverlay title={Strings.wait} />
                }

            

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser: state.auth.currentUser,
   
})


export default connect(mapToStateProps, mapDispatchToProps)(ShowIntialRoport);

