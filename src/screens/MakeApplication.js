import React, { Component } from 'react';
import {
    View,FlatList,RefreshControl,ActivityIndicator, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppHomeHeader from '../components/AppHomeHeader'
import Menu from '../components/Menu'
import Checkbox from 'react-native-custom-checkbox';
//import DateTimePicker from '@react-native-community/datetimepicker';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import DocumentPicker from 'react-native-document-picker';
import moment from 'moment'
import ImagePicker from 'react-native-image-crop-picker';


class MakeApplication extends Component {
    state={
        patientName:' ',
        patientCode:' ',
       
        page:this.props.data?this.props.data.page:1,
        apllicationId:this.props.data?this.props.data.applicationId:null,
        applicationsStatus:this.props.data?this.props.data.applicationsStatus:null,
        
        agree:false,
        count:11,

        showMediaType:false,
        mediaType:null, //for check
        selectedMeida:null, //for box title
        media:[], //for ulpoad
        allMedia:[],//for show all media

        showCalender:false,
        date:new Date(),
        selectedDate:null,
        agreeDate:false,

        check1:true,
        check2:true,
        check3:true,
        check4:true,
        check5:true,
        check6:true,
        check7:true,
        check8:true,
        check9:true,
        check10:true,
        check11:true,

        loading:false,
        changeStateAndUploadFiles:false,
        ConfirmAllDocumentsUploaded:false,
    }

    componentDidMount(){
        moment.locale('en')
        console.log("props   ",this.props.data)
        console.log("props2   ",this.state.apllicationId)
    }

    createApplication = () =>{
        const {currentUser} = this.props
        this.setState({loading:true})
        var appName=''
        //check type of user
        if(currentUser.role=='doctor'||currentUser.role=='cc'){
            appName=`${this.state.patientName}-${moment(new Date()).format('DD/MM/YYYY')}`
        }else{
            appName=`${currentUser?currentUser.first_name+" "+currentUser.last_name:''}-${moment(new Date()).format('DD/MM/YYYY')}`
        }
        const data = new URLSearchParams()
        data.append('name',appName)
        data.append('userId',this.props.currentUser.id)
        data.append('status','created')
        //data.append('doctorId','1')
        data.append('isPayment',false)
        if(currentUser.role=='cc'){
            data.append('code',this.state.patientCode)
        }
        axios.post(`${BASE_END_POINT}applications/`,data,{
            headers: {
                "Content-Type": 'application/x-www-form-urlencoded',
                "Authorization": `authorization ${this.props.currentUser.token}`
            }
        })
        .then(response=>{
            RNToasty.Success({title:Strings.yourApplicationCreatedSuccessfuly})
            console.log('APP CRATED   ',response)
            this.setState({apllicationId:response.data.data.id,page:2,loading:false})
        })
        .catch(error=>{
            this.setState({loading:false})
            console.log('error   ',error)
        })
    }

    uploadApplicationFiles = () =>{
        this.setState({loading:true})
        const data = new FormData()
        data.append('applicationId',this.state.apllicationId)
         data.append('issueDate',this.state.selectedDate)
        this.state.media.map(file=>{
            /*data.append('document',{
                uri: file,
                type: 'multipart/form-data',
                name: 'productImages'
            })*/
            data.append('document',file)    
        })
        
        axios.post(`${BASE_END_POINT}files/upload/`,data,{
            headers: {
                "Content-Type": 'multipart/form-data',
                "Authorization": `authorization ${this.props.currentUser.token}`
            }
        })
        .then(response=>{
            RNToasty.Success({title:Strings.youFilesUploadedSuccessfuly})
            console.log('File Uploades   ',response)
            console.log('id   ',this.state.apllicationId)
            this.setState({allMedia:[...this.state.allMedia,...this.state.media], media:[]})
            if(this.state.applicationsStatus==null){
                this.applicationStatusChannge(this.state.apllicationId,"Specialist-Review")
            }else{
                this.applicationStatusChannge(this.state.apllicationId,"IR-CallConfirm")
                //this.setState({loading:false,changeStateAndUploadFiles:true})
            }
        })
        .catch(error=>{
            this.setState({loading:false})
            console.log('error in file upload   ',error.response)
        })
    }

    applicationStatusChannge = (id,appState) =>{
        console.log("id3   ",id)
        //var data= new URLSearchParams()
        //data.append('status','Specialist-Review')
        //data.append('id',id)
        var data={
            status:appState,
            'id':id
        }
        axios.put(`${BASE_END_POINT}applications`,data,{
            headers: {
                "Content-Type": 'application/json',
                "Authorization": `authorization ${this.props.currentUser.token}`
            }
        })
        .then(response=>{
            RNToasty.Success({title:Strings.youFilesUploadedSuccessfuly})
            console.log('change application status   ',response)
            this.setState({loading:false,changeStateAndUploadFiles:true})
        })
        .catch(error=>{
            this.setState({loading:false})
            console.log('error   ',error.response)
            console.log('error2   ',error)
        })
    }

    nameInput = () => {
        const { isRTL } = this.props
        const { patientName } = this.state
        return (
            <View style={{marginTop: moderateScale(7), width: wp(90), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{  borderBottomColor:colors.darkGray,borderBottomWidth:0.5,  flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(90)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.name}</Label>    
                    <Input
                        onChangeText={(val) => { this.setState({ patientName: val }) }}
                        style={{fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(90), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.name}
                    />
                </Item>

                {patientName.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }

    patientCodeInput = () => {
        const { isRTL } = this.props
        const { patientCode } = this.state
        return (
            <View style={{marginTop: moderateScale(7), width: wp(90), alignSelf: 'center', justifyContent: 'center', alignItems: 'center', }}>
                
                <Item floatingLabel style={{  borderBottomColor:colors.darkGray,borderBottomWidth:0.5,  flexDirection: isRTL ? 'row-reverse' : 'row', alignItems: 'center', width: wp(90)}}>

                    <Label  style={{color:colors.darkGray}} >{Strings.patientCode}</Label>    
                    <Input
                        onChangeText={(val) => { this.setState({ patientCode: val }) }}
                        style={{fontSize:responsiveFontSize(5),textAlign: isRTL ? 'right' : 'left', width: wp(90), fontFamily: isRTL ? arrabicFont : englishFont, height: responsiveHeight(7) }}
                        placeholder={Strings.patientCode}
                    />
                </Item>

                {patientCode.length == 0 &&
                <Text style={{ color: 'red', fontSize: responsiveFontSize(6), alignSelf: isRTL ? 'flex-start' : 'flex-end' }}> {Strings.require}</Text>
                }
            </View>
        )
    }


    page1=()=>{
        const {isRTL,currentUser} = this.props
        return (
            <View  style={{flex:1,backgroundColor:'transparent',marginTop:moderateScale(6)}}>
                 
                 
                
                <View style={{marginTop:moderateScale(5), alignSelf:'center', width:wp(96),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <Checkbox     
                    checked={this.state.check1}
                    style={{marginHorizontal:moderateScale(6), borderColor:'#F79EFE', backgroundColor:'white',color:'#946AB3',borderRadius:moderateScale(2)}}
                    onChange={(name, checked) => {
                        this.setState({ check1: true });
                    }}
                    />
                    <Text style={{textAlign:isRTL?'right':'left', width:wp(90), marginHorizontal:moderateScale(1), fontSize:responsiveFontSize(6), color:'#818181', fontFamily:boldFont}}>{Strings.check1} </Text>
                </View>

                <View style={{marginTop:moderateScale(5), alignSelf:'center', width:wp(96),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <Checkbox     
                    checked={this.state.check2}
                    style={{marginHorizontal:moderateScale(6), borderColor:'#F79EFE', backgroundColor:'white',color:'#946AB3',borderRadius:moderateScale(2)}}
                    onChange={(name, checked) => {
                        this.setState({ check2: true });
                    }}
                    />
                    <Text style={{textAlign:isRTL?'right':'left', width:wp(90), marginHorizontal:moderateScale(1), fontSize:responsiveFontSize(6), color:'#818181', fontFamily:boldFont}}>{Strings.check2} </Text>
                </View>
              

                <View style={{marginTop:moderateScale(5), alignSelf:'center', width:wp(96),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <Checkbox            
                    checked={this.state.check3}
                    style={{marginHorizontal:moderateScale(6), borderColor:'#F79EFE', backgroundColor:'white',color:'#946AB3',borderRadius:moderateScale(2)}}
                    onChange={(name, checked) => {
                        this.setState({ check3: true });
                    }}
                    />
                    <Text style={{textAlign:isRTL?'right':'left',width:wp(90), marginHorizontal:moderateScale(1), fontSize:responsiveFontSize(6), color:'#818181', fontFamily:boldFont}}>{Strings.check3} </Text>
                </View>
              

            
                <View style={{position:'absolute',bottom:moderateScale(5),left:0 ,justifyContent:'space-between', alignSelf:'center', width:wp(100),flexDirection:'row',alignItems:'center'}}>
                   <TouchableOpacity
                   onPress={()=>{
                       this.setState({agree:!this.state.agree})
                   }}
                    style={{marginHorizontal:moderateScale(5), flexDirection:'row',alignItems:'center'}}>
                         <View style={{justifyContent:'center',alignItems:'center', width:20,height:20,borderRadius:10,backgroundColor:colors.darkBlue}}>
                            {this.state.agree&&<Icon name='check' type='AntDesign' style={{color:'white',fontSize:responsiveFontSize(5)}} />}
                         </View>
                         <Text style={{ marginHorizontal:moderateScale(2), fontSize:responsiveFontSize(7), color:colors.darkBlue, fontFamily:englishFont}}>{Strings.agreeTerms}</Text>
                   </TouchableOpacity>
                   
                   <TouchableOpacity 
                   onPress={()=>{
                       if(this.state.agree){
                           if(currentUser.role=='patient'){
                            this.createApplication()
                           }else if(currentUser.role=='doctor'){
                                if (!this.state.patientName.replace(/\s/g, '').length) {
                                    this.setState({ patientName: '' })
                                }else{
                                    this.createApplication()
                                }
                           }else if(currentUser.role=='cc'){
                                if (!this.state.patientName.replace(/\s/g, '').length) {
                                    this.setState({ patientName: '' })
                                }
                                if (!this.state.patientCode.replace(/\s/g, '').length) {
                                    this.setState({ patientCode: '' })
                                }
                                if (this.state.patientName.replace(/\s/g, '').length&&this.state.patientCode.replace(/\s/g, '').length) {
                                    this.createApplication()
                                }
                       }
                       }else{
                           RNToasty.Error({title:Strings.mustAgree})
                       }   
                   }}
                   style={{ justifyContent:'center', borderTopLeftRadius:moderateScale(20),borderBottomLeftRadius:moderateScale(20), width:wp(30),height:responsiveHeight(11),backgroundColor:'#946AB3', flexDirection:'row',alignItems:'center'}}>
                        <Text style={{ marginHorizontal:moderateScale(2), fontSize:responsiveFontSize(7), color:'white', fontFamily:englishFont}}>{Strings.next} </Text>
                        <Icon name='arrowright' type='AntDesign' style={{color:'white',fontSize:responsiveFontSize(10)}} />
                   </TouchableOpacity>
                </View>
                   
            </View>
        )
    }
  


    pickMedia = async () =>{
        try {
            const results = await DocumentPicker.pickMultiple({
              type:[DocumentPicker.types.allFiles], //[DocumentPicker.types.images,DocumentPicker.types.video,DocumentPicker.types.audio],
            });

            console.log("ME   ",results)
            this.setState({media:[...this.state.media,...results]})    
        } catch (err) {
            console.log("errror   ",err)
        }
    }

    pickMediaFromPicker = async  () =>{
        ImagePicker.openCamera({
            width: 800,
            height: 800,
            //cropping: true,
          }).then(image => {
            console.log(" camer img   ",image);
            const i = {
                name:'image_'+new Date().getTime()+'.png',
                type:image.mime,
                size:image.size,
                uri:image.path,
            }
            console.log("I   ",image)
            this.setState({media:[...this.state.media,i]}) 
          }).catch(error=>{
              console.log("Error   ",error)
          })
          ;
    }

   

    mediaTypeBox = ()=>{
        const {isRTL} = this.props
        const {showMediaType,mediaType,selectedMeida} = this.state
        return(
            <View style={{width:wp(80), alignSelf:'center',marginTop:moderateScale(5)}}>
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({showMediaType:!showMediaType})
                }}
                 style={{width:wp(80),height:responsiveHeight(6),borderRadius:moderateScale(6),borderWidth:2,borderColor:'#946AB3',justifyContent:'space-between',alignItems:'center',flexDirection:isRTL?'row-reverse':'row'}}>
                    {mediaType?
                    <Text style={{marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:'#946AB3', fontFamily:englishFont}}>{selectedMeida} </Text>
                    :
                    <Text style={{marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:'#946AB3', fontFamily:englishFont}}>{Strings.docType} </Text>
                    }
                    <View style={{marginHorizontal:moderateScale(3)}}>
                    <Icon name='caretdown' type='AntDesign' style={{color:'#946AB3',fontSize:responsiveFontSize(8)}} />
                    </View>
                </TouchableOpacity>
                
                {showMediaType&&
                <View style={{marginTop:moderateScale(2),width:wp(80),borderRadius:moderateScale(6),borderWidth:2,borderColor:'#946AB3'}}>
                    <TouchableOpacity 
                    onPress={()=>{
                        this.pickMedia()
                        this.setState({ showMediaType:false, selectedMeida:Strings.photo,mediaType:'photo'})
                    }}
                    style={{borderTopLeftRadius:moderateScale(6),borderTopRightRadius:moderateScale(6), justifyContent:'center',backgroundColor:mediaType=='photo'?'#F79EFE':'tranparent', flex:1,height:responsiveHeight(5),}}>
                        <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:mediaType=='photo'?'white':'#946AB3', fontFamily:englishFont}}>{Strings.photo} </Text>
                    </TouchableOpacity>

                    <TouchableOpacity 
                    onPress={()=>{
                       // this.pickMedia()
                       this.pickMediaFromPicker()
                        this.setState({showMediaType:false, selectedMeida:Strings.camera,mediaType:'camera'})
                    }}
                    style={{justifyContent:'center',backgroundColor:mediaType=='camera'?'#F79EFE':'tranparent', flex:1,height:responsiveHeight(5),}}>
                        <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:mediaType=='camera'?'white':'#946AB3', fontFamily:englishFont}}>{Strings.camera} </Text>
                    </TouchableOpacity>

                    <TouchableOpacity 
                    onPress={()=>{
                        this.pickMedia()
                        this.setState({showMediaType:false, selectedMeida:Strings.video,mediaType:'video'})
                    }}
                    style={{justifyContent:'center',backgroundColor:mediaType=='video'?'#F79EFE':'tranparent', flex:1,height:responsiveHeight(5),}}>
                        <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:mediaType=='video'?'white':'#946AB3', fontFamily:englishFont}}>{Strings.video} </Text>
                    </TouchableOpacity>

                    <TouchableOpacity 
                    onPress={()=>{
                        this.pickMedia()
                        this.setState({showMediaType:false, selectedMeida:Strings.voiceNote,mediaType:'voice'})
                    }}
                    style={{borderBottomLeftRadius:moderateScale(6),borderBottomRightRadius:moderateScale(6),justifyContent:'center', flex:1,backgroundColor:mediaType=='voice'?'#F79EFE':'tranparent',height:responsiveHeight(5),}}>
                        <Text style={{alignSelf:isRTL?'flex-end':'flex-start', marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:mediaType=='voice'?'white':'#946AB3', fontFamily:englishFont}}>{Strings.voiceNote} </Text>
                    </TouchableOpacity>
                </View>
                }
            </View>
        )
    }

    mediaItem = (val,uploaded) =>{
        const {isRTL} = this.props
        const {media,allMedia} = this.state
        return(
            val.map((file,index)=>(
            <View style={{width:wp(80),alignSelf:'center',marginTop:moderateScale(5),flexDirection:'row',alignItems:'center',justifyContent:'space-between'}}>
                <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center'}}>
                    <Icon name='attachment' type='Entypo' style={{color:colors.darkGray ,fontSize:responsiveFontSize(10)}} />
                    <Text style={{maxWidth:wp(60), marginHorizontal:moderateScale(5), fontSize:responsiveFontSize(7), color:colors.darkBlue, fontFamily:englishFont}}>{file.name}</Text>
                </View>
                
                {!uploaded&&
                <TouchableOpacity
                onPress={()=>{
                    var removedMedia = media.splice(index,1)
                    console.log("media    ",media)
                    this.setState({media:media})
                }}
                >
                    <Icon name='delete' type='MaterialCommunityIcons' style={{color:colors.darkGray ,fontSize:responsiveFontSize(12)}} />
                </TouchableOpacity>
                }       
            </View>
            ))
        )
    }

    uploadButton=()=>{
        const {isRTL} = this.props
        const {media,selectedDate,agreeDate} = this.state
        return (
            <TouchableOpacity 
            onPress={()=>{
              if(media.length==0){
                RNToasty.Warn({title:Strings.pleaseSelectFiles,duration:1})
              }else if(!selectedDate) {
                RNToasty.Warn({title:Strings.pleaseSelectDate,duration:1})
             }else if(agreeDate==false) {
                RNToasty.Warn({title:Strings.pleaseAgreeDate,duration:1})
             }else{
                this.uploadApplicationFiles()
              }
            }}
            style={{alignSelf:'center',marginTop:moderateScale(5), justifyContent:'center', borderRadius:moderateScale(6), width:wp(80),height:responsiveHeight(6),backgroundColor:'#946AB3', alignItems:'center'}}>
                 <Text style={{ marginHorizontal:moderateScale(2), fontSize:responsiveFontSize(7), color:'white', fontFamily:englishFont}}>{Strings.upload} </Text>
            </TouchableOpacity>
        )
    }

    calender=()=>{
        const {isRTL} = this.props
        const {showCalender,date, selectedDate} = this.state
        return (
            <View style={{width:wp(80), alignSelf:'center',}}>
                <Text style={{alignSelf:isRTL?'flex-end':'flex-start',marginTop:moderateScale(5), fontSize:responsiveFontSize(6), color:'black', fontFamily:englishFont}}>{Strings.documentIssueDate} </Text>
                
                <TouchableOpacity 
                onPress={()=>{
                    this.setState({showCalender:true,})
                    //this.setState({showMediaType:!showMediaType})
                }}
                 style={{width:wp(80),marginTop:moderateScale(3), height:responsiveHeight(6),borderTopLeftRadius:moderateScale(6),borderTopRightRadius:moderateScale(6),borderWidth:2,borderColor:'#946AB3',justifyContent:'space-between',alignItems:'center',flexDirection:isRTL?'row-reverse':'row'}}>
                    
                    <Text style={{marginHorizontal:moderateScale(3), fontSize:responsiveFontSize(6), color:'#946AB3', fontFamily:englishFont}}>{selectedDate?selectedDate:Strings.selectedDate}</Text>
                    
                    <View style={{marginHorizontal:moderateScale(3)}}>
                    <Icon name='calendar' type='Feather' style={{color:'#946AB3',fontSize:responsiveFontSize(8)}} />
                    </View>
                </TouchableOpacity>

                <TouchableOpacity
                   onPress={()=>{
                       this.setState({agreeDate:!this.state.agreeDate})
                   }}
                    style={{alignSelf:isRTL?'flex-end':'flex-start',marginTop:moderateScale(5), flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                         <View style={{justifyContent:'center',alignItems:'center', width:20,height:20,borderRadius:10,backgroundColor:colors.darkBlue}}>
                            {this.state.agreeDate&&<Icon name='check' type='AntDesign' style={{color:'white',fontSize:responsiveFontSize(5)}} />}
                         </View>
                         <Text style={{ marginHorizontal:moderateScale(2), fontSize:responsiveFontSize(6), color:colors.darkBlue, fontFamily:boldFont}}>{Strings.documentIssueDateConfirmed}</Text>
                </TouchableOpacity>
            
                {this.state.showCalender&&
                <DateTimePickerModal
        
                value={date}
                maximumDate={new Date()}
                is24Hour={true}
                isVisible={this.state.showCalender}
                mode='date'
                onConfirm={(date)=>{
                    console.log("DATE   ",date)
                    const d = moment(date).format('DD-MM-YYYY')
                    console.log("DAT   ",d)
                    this.setState({showCalender:false, selectedDate:d,date:date})
                }}
                onCancel={()=>{
                    this.setState({showCalender:false})
                }}
                />
                }
            </View>
        )
    }
    
    page2=()=>{
        const {isRTL} = this.props
        return (
            <ScrollView showsVerticalScrollIndicator={false} style={{flex:1,backgroundColor:'transparent',marginTop:moderateScale(6)}}>

                <Text style={{alignSelf:'center', fontSize:responsiveFontSize(9), color:'black', fontFamily:englishFont}}>{Strings.uploadDocs} </Text>
                {this.mediaTypeBox()}
                {this.mediaItem(this.state.media)}
                {this.calender()}
                {this.uploadButton()}
                {this.mediaItem(this.state.allMedia,true)}

                <View style={{marginTop:moderateScale(5), alignSelf:'center', width:wp(80),flexDirection:isRTL?'row-reverse':'row',alignItems:'center'}}>
                    <Checkbox     
                    checked={this.state.ConfirmAllDocumentsUploaded}
                    style={{marginHorizontal:moderateScale(6), borderColor:'#F79EFE', backgroundColor:'white',color:'#946AB3',borderRadius:moderateScale(2)}}
                    onChange={(name, checked) => {
                        this.setState({ ConfirmAllDocumentsUploaded: checked });
                    }}
                    />
                    <Text style={{textAlign:isRTL?'right':'left', width:wp(70), marginHorizontal:moderateScale(1), fontSize:responsiveFontSize(6), color:'#818181', fontFamily:boldFont}}>{Strings.check1} </Text>
                </View>

                <View style={{marginVertical:moderateScale(15),justifyContent:'space-between', alignSelf:'center', width:wp(100),flexDirection:'row',alignItems:'center'}}>
                   
                <TouchableOpacity 
                   onPress={()=>{
                       /*if(this.props.data){
                        pop()
                       }else{
                        this.setState({page:1})
                       }*/
                       pop() 
                   }}
                   style={{ justifyContent:'center', borderTopRightRadius:moderateScale(20),borderBottomRightRadius:moderateScale(20), width:wp(30),height:responsiveHeight(11),backgroundColor:'#946AB3', flexDirection:'row',alignItems:'center'}}>
                        <Text style={{ marginHorizontal:moderateScale(2), fontSize:responsiveFontSize(7), color:'white', fontFamily:englishFont}}>{Strings.back} </Text>
                        <Icon name='arrowleft' type='AntDesign' style={{color:'white',fontSize:responsiveFontSize(10)}} />
                   </TouchableOpacity>
                   
                   <TouchableOpacity 
                   onPress={()=>{
                       if(this.state.changeStateAndUploadFiles){
                         
                        if(this.state.ConfirmAllDocumentsUploaded){
                            if(this.state.applicationsStatus==null){
                                this.setState({page:3})
                                //push('DetermineApplicationContactTimeWithSpecialist',{applicationId:this.state.apllicationId})
                           }else{
                                this.setState({page:3})
                                //push('SpecialListReviewApplication')
                           }
                        }else{
                            RNToasty.Warn({title:Strings.pleaseConfirmAllDocumentsUploaded,duration:1})
                        }
                           
                
                       }else{
                        RNToasty.Warn({title:Strings.uploadFilesState})
                       }
                   }}
                   style={{ justifyContent:'center', borderTopLeftRadius:moderateScale(20),borderBottomLeftRadius:moderateScale(20), width:wp(30),height:responsiveHeight(11),backgroundColor:'#946AB3', flexDirection:'row',alignItems:'center'}}>
                        <Text style={{ marginHorizontal:moderateScale(2), fontSize:responsiveFontSize(7), color:'white', fontFamily:englishFont}}>{Strings.next} </Text>
                        <Icon name='arrowright' type='AntDesign' style={{color:'white',fontSize:responsiveFontSize(10)}} />
                   </TouchableOpacity>
                </View>

            </ScrollView>
        )
    }
    
    page3 = () =>{
        return(
            <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                <FastImage
                resizeMode='contain'
                source={require('../assets/imgs/success.png')}
                style={{width:responsiveWidth(40),height:responsiveHeight(22)}}
                />
                <Text style={{marginTop:moderateScale(5), width:responsiveWidth(80),textAlign:'center', fontFamily:arrabicFont,fontSize:responsiveFontSize(9)}} >{Strings.thankYouForUploadingYourFilesWeWillContactYouSoon}</Text>
            </View>
        )
    }
    // <Text style={{width:wp(60),textAlign:'center', marginTop:moderateScale(5),fontSize:responsiveFontSize(9), color:'#707070', fontFamily:boldFont}}>{Strings.wait24Hour}</Text>

    render() {
        const {isRTL} = this.props
        const {page} = this.state 
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                <AppHomeHeader />
                {page==1&&this.page1()}
                {page==2&&this.page2()}
                {page==3&&this.page3()}

                {this.state.loading&&<LoadingDialogOverlay title={Strings.wait}/>}
                {this.props.showMenuModal&&<Menu/>}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser: state.auth.currentUser,
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(MakeApplication);

