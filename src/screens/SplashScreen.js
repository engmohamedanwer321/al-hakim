import React, {Component} from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {View, TouchableOpacity, Text} from 'react-native';
import {connect} from 'react-redux';
import {
  responsiveHeight,
  responsiveWidth,
  responsiveFontSize,
  moderateScale,
} from '../utils/responsiveDimensions';
import Strings from '../assets/strings';
import {setUser, userToken} from '../actions/AuthActions';
import {changeLanguage, changeColor} from '../actions/LanguageActions';
import {enableSideMenu, push, resetTo} from '../controlls/NavigationControll';
import {selectMenu} from '../actions/MenuActions';
import FastImage from 'react-native-fast-image';
import {Button} from 'native-base';
import {arrabicFont, englishFont, boldFont} from '../common/AppFont';
import * as colors from '../assets/colors';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {AnimatedCircularProgress} from 'react-native-circular-progress';

class SplashScreen extends Component {


  componentDidMount() {
    this.checkLanguage();
    enableSideMenu(false, null);
    // setTimeout(()=>{ this.checkLogin()},3000)
  }

  checkLogin = async () => {
    const userJSON = await AsyncStorage.getItem('USER');
    if (userJSON) {
      console.log('Exist User');
      const userInfo = JSON.parse(userJSON);
      this.props.setUser(userInfo);
      console.log(userInfo);
      if(userInfo.role=='administration'){ //administration
        resetTo('AdminstraionHome')
      }else{
        resetTo('Home')
      }
    } else {
      console.log('no User');
      resetTo('Login');
    }
  };

  checkLanguage = async () => {
    console.log('lang0   ' + lang);
    const lang = await AsyncStorage.getItem('@lang');
    console.log('lang   ' + lang);
    if (lang === 'ar') {
      this.props.changeLanguage(true);
      Strings.setLanguage('ar');
    } else {
      this.props.changeLanguage(false);
      Strings.setLanguage('en');
    }
  };
  //source={require('../assets/imgs/splashBackground.png')}

  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <FastImage
          resizeMode="contain"
          source={require('../assets/imgs/appLogo.png')}
          style={{width: wp(80), height: responsiveHeight(30)}}
        />

        <AnimatedCircularProgress
          size={170}
          width={13}
          fill={100}
          duration={5000}
          onAnimationComplete={() => {
            console.log('Done');
            this.checkLogin();
            //resetTo('Login')
          }}
          tintColor={colors.darkBlue}
          backgroundColor={colors.lightBlue}>
          {fill => (
            <View
              style={{
                width: 170,
                height: 170,
                borderRadius: 85,
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth: 13,
                borderColor: colors.darkBlue,
              }}>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: 140,
                  height: 140,
                  borderRadius: 70,
                  backgroundColor: colors.lightBlue,
                }}>
                <FastImage
                  source={require('../assets/imgs/ribbon.png')}
                  style={{width: 80, height: 80}}
                />
              </View>
            </View>
          )}
        </AnimatedCircularProgress>

        {/* <TouchableOpacity style={{alignSelf:'center'}}>
                    <View style={{width:170,height:170,borderRadius:85,justifyContent:'center',alignItems:'center',borderWidth:13,borderColor:colors.darkBlue}}>
                        <View style={{justifyContent:'center',alignItems:'center', width:140,height:140,borderRadius:70,backgroundColor:colors.lightBlue}}>
                            <FastImage
                            source={require('../assets/imgs/ribbon.png')}
                            style={{width:80,height:80}}
                            />
                        </View>
                    </View>
                </TouchableOpacity>
            */}

        <Text
          style={{
            fontFamily: boldFont,
            color: colors.darkBlue,
            marginTop: moderateScale(5),
            fontSize: responsiveFontSize(5),
          }}>
          POWERED BY
        </Text>

        <FastImage
          source={require('../assets/imgs/hca.jpg')}
          style={{marginTop: moderateScale(1),width: wp(36),height: responsiveHeight(18),}}
        />

      </View>
    );
  }
}

const mapToStateProps = state => ({
  currentUser: state.auth.currentUser,
  userTokens: state.auth.userToken,
});

const mapDispatchToProps = {
  setUser,
  changeLanguage,
  userToken,
  selectMenu,
};

export default connect(
  mapToStateProps,
  mapDispatchToProps,
)(SplashScreen);
