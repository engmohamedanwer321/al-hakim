import React, { Component } from 'react';
import {
    View,FlatList,RefreshControl,ActivityIndicator,Easing, TouchableOpacity, Image, Text, ScrollView, TextInput, Alert, Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Icon, Button,Item, Input, Label } from 'native-base';
import { setUser } from '../actions/AuthActions'
import { responsiveHeight, responsiveWidth, moderateScale, responsiveFontSize } from "../utils/responsiveDimensions";
import Strings from '../assets/strings';
import LoadingDialogOverlay from '../components/LoadingDialogOverlay';
import { enableSideMenu, resetTo, push,pop } from '../controlls/NavigationControll'
import { arrabicFont, englishFont,boldFont } from '../common/AppFont'
import * as Animatable from 'react-native-animatable';
import * as colors from '../assets/colors';
import { removeItem } from '../actions/MenuActions';
import FastImage from 'react-native-fast-image'
import { RNToasty } from 'react-native-toasty';
import AsyncStorage from '@react-native-community/async-storage'
import axios from 'axios';
import { BASE_END_POINT } from '../AppConfig';
import strings from '../assets/strings';
import {heightPercentageToDP as hp, widthPercentageToDP as wp} from 'react-native-responsive-screen'
import AppHomeHeader from '../components/AppHomeHeader'
import Menu from '../components/Menu'
import ApllicationCard from '../components/ApllicationCard'
import { AnimatedCircularProgress } from 'react-native-circular-progress';
import Spiner from '../common/Spiner'

class SpecialListReviewApplication extends Component {

    componentDidMount(){
        this.circularProgress.animate(100, 1000,Easing.linear);
      
        this.intervalId = setInterval(
          () => this.circularProgress.reAnimate(0,100, 1000,Easing.linear),
          1000
        );
      }
      
      componentWillUnmount() {
        clearInterval(this.intervalId);
      }

   


    render() {
        const {isRTL} = this.props
        return (
            <View style={{ backgroundColor:colors.lightGray,flex:1}}>
                <AppHomeHeader />
                <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    
                    <AnimatedCircularProgress
                    size={120}
                    width={13}
                    fill={0}
                    //duration={1000}
                    ref={(ref) => this.circularProgress = ref}
                    arcSweepAngle={360}
                    rotation={360}
                    lineCap="round"
                    onAnimationComplete={()=>{
                       
                    }}
                    tintColor='#946AB3'
                    backgroundColor='transparent'
                    />
                   
                    <Text style={{width:wp(60),textAlign:'center', marginTop:moderateScale(5),fontSize:responsiveFontSize(9), color:'#707070', fontFamily:boldFont}}>{Strings.wait24Hour}</Text>

                </View>
                {this.props.showMenuModal&&<Menu/>}

            </View>
        );
    }
}
const mapDispatchToProps = {
    setUser,
    removeItem
}

const mapToStateProps = state => ({
    isRTL: state.lang.RTL,
    showMenuModal:state.menu.showMenuModal,
    currentUser:state.auth.currentUser, 
    //userToken: state.auth.userToken,
})


export default connect(mapToStateProps, mapDispatchToProps)(SpecialListReviewApplication);

