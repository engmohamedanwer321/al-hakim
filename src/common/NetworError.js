//
import React, { Component } from "react";
import { Text, View} from "react-native";
import LottieView from 'lottie-react-native';
import { moderateScale, responsiveWidth, responsiveHeight,responsiveFontSize } from '../utils/responsiveDimensions';
import { connect } from "react-redux";


class NetworError extends Component {
render(){
    return(
        <View style={{alignSelf:'center',marginTop:moderateScale(20), backgroundColor:'transparent',justifyContent:'center',alignItems:'center'}}>
            <LottieView
            style={{width:responsiveWidth(50)}}
            source={require('../assets/animations/netWorkError.json')}
            autoPlay
            loop
            />
            <Text style={{marginTop:moderateScale(0), fontSize:responsiveFontSize(7),fontWeight:'700'}} >NETWORK ERROR</Text>
        </View>
    )
}
}




const mapStateToProps = state => ({
    isRTL: state.lang.RTL,
});

export default connect(mapStateToProps)(NetworError);
